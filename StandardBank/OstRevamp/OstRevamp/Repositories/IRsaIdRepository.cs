﻿using OstRevamp.Models;
using OstRevamp.Repositories;

namespace OtsRevamp.Repositories
{
    public interface IRsaIdRepository : IRepository<RsaId>
    {
        RsaId GetByIdNumber(string userId);
    }
}
