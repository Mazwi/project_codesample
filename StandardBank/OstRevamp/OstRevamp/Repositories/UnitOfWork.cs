﻿using OstRevamp.Models;
using OtsRevamp.Repositories;

namespace OstRevamp.Repositories
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly OstContext _context;

        public IRsaIdRepository RsaIds { get; private set; }

        public UnitOfWork(OstContext context)
        {
            _context = context;
            RsaIds = new RsaIdRepository(_context);
        }

        public int Complete()
        {
            return _context.SaveChanges();
        }

        public void Dispose()
        {
            _context.Dispose();
        }
    }
}
