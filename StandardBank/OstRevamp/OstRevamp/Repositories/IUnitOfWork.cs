﻿using OtsRevamp.Repositories;
using System;

namespace OstRevamp.Repositories
{
    public interface IUnitOfWork : IDisposable
    {
        IRsaIdRepository RsaIds { get; }
        int Complete();
    }
}