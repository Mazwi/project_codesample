﻿using OstRevamp.Models;
using OtsRevamp.Repositories;
using System.Linq;

namespace OstRevamp.Repositories
{
    public class RsaIdRepository : Repository<RsaId>, IRsaIdRepository
    {
        public RsaIdRepository(OstContext context) : base(context)
        {
        }

        public RsaId GetByIdNumber(string idNumber)
        {
            RsaId rsaId = OstContext.RsaIds.SingleOrDefault(i => i.IdNumber == idNumber);
            return rsaId;
        }

        public OstContext OstContext
        {
            get { return Context as OstContext; }
        }
    }
}
