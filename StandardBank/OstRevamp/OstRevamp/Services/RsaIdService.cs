﻿using OstRevamp.Models;
using OstRevamp.Repositories;
using System;

namespace OstRevamp.Services
{
    public class RsaIdService
    {
        private UnitOfWork _unitOfWork;

        public RsaIdService()
        {
            _unitOfWork = new UnitOfWork(new OstContext());
        }
        
        public void CreateRsaId(RsaIdViewModel rsaIdViewModel)
        {
            var rsaId = new RsaId();
            rsaId.IdNumber = rsaIdViewModel.IdNumber;
            rsaId.DateOfBirth = new DateTime(rsaIdViewModel.YearOfBirth.Value, 
                                                rsaIdViewModel.MonthOfBirth.Value, 
                                                rsaIdViewModel.DayOfBirth.Value);
            var genderDigits = Convert.ToInt32(rsaIdViewModel.IdNumber.Substring(6, 4));
            rsaId.Gender = (genderDigits < 5000) ? GenderEnum.Female : GenderEnum.Male;
            rsaId.Citizenship = rsaId.IdNumber.Substring(10, 1) == "0" ? "South African" : "Not South African";
                
            _unitOfWork.RsaIds.Add(rsaId);
            _unitOfWork.Complete();
        }

        public RsaIdViewModel GetByIdNumber(string idNumber)
        {
            RsaId rsaId = _unitOfWork.RsaIds.GetByIdNumber(idNumber);
            if (rsaId == null) return null;
            var rsaIdViewModel = new RsaIdViewModel(rsaId.Id, rsaId.IdNumber, rsaId.DateOfBirth, rsaId.Gender, rsaId.Citizenship);

            return rsaIdViewModel;
        }

        public static RsaIdService CreateRsaIdService()
        {
            return new RsaIdService();
        }

    }
}