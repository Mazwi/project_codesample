﻿using OstRevamp.Models;
using OstRevamp.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text.RegularExpressions;
using System.Web.Http;
using System.Web.Http.Cors;
using System.Web.Http.Description;

namespace OstRevamp.Controllers
{
    public class TestObj
    {
        public int Id;
        public string Title;
    }

    [EnableCors(origins: "http://localhost:8050", headers: "*", methods: "*")]
    public class RsaIdController : ApiController
    {
        // POST api/values
        private RsaIdService _rsaIdService;

        public RsaIdController()
        {
            _rsaIdService = new RsaIdService();
        }

        public HttpResponseMessage Post(RsaIdViewModel rsaId)
        {
            try
            {
                string validationResults = ValidateIdNumber(rsaId.IdNumber);
                if (validationResults != "OK")
                {
                    return Request.CreateResponse(HttpStatusCode.NotAcceptable, validationResults);
                }

                _rsaIdService.CreateRsaId(rsaId);

                RsaIdViewModel rsaIdDetails = _rsaIdService.GetByIdNumber(rsaId.IdNumber);
                return Request.CreateResponse(HttpStatusCode.OK, rsaIdDetails);
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ex);
            }
        }

        public string ValidateIdNumber(string idNumber)
        {
            if(string.IsNullOrEmpty(idNumber)) return "Id Number connot be blank";
            if(idNumber.Length != 13) return "Id Number must be 13 characters long";
            if(!idNumber.All(char.IsDigit)) return "Id Number must be numbers only";
            
            string regexIdPattern = "(?<Year>[0-9][0-9])(?<Month>([0][1-9])|([1][0-2]))(?<Day>([0-2][0-9])|([3][0-1]))(?<Gender>[0-9])(?<Series>[0-9]{3})(?<Citizenship>[0-9])(?<Uniform>[0-9])(?<Control>[0-9])";

            var idPattern = new Regex(regexIdPattern);
            bool isValidFormat = false;
            if (idPattern.IsMatch(idNumber))
            {
                if (idNumber.Substring(2, 2) != "00" && idNumber.Substring(4, 2) != "00")
                {
                    isValidFormat = true;
                }
            }
            if (!isValidFormat) return "Invalid Id Number";

            if(_rsaIdService.GetByIdNumber(idNumber) != null) return "Id Number already registered";

            return "OK";
        }
    }
}
