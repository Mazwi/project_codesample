﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OstRevamp.Models
{
    public enum GenderEnum
    {
        None = 0,
        Male = 1,
        Female = 2
    }
}