﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace OstRevamp.Models
{
    public class RsaIdViewModel
    {
        public int Id { get; set; }

        [StringLength(13, ErrorMessage = "The {0} must be at {2} characters long.", MinimumLength = 13)]
        [DataType(DataType.Text)]
        [Display(Name = "Id Number")]
        public string IdNumber { get; set; }

        [Display(Name = "Year Born")]
        public int? YearOfBirth { get; set; }

        [Display(Name = "Month Born")]
        public int? MonthOfBirth { get; set; }

        [Display(Name = "Day of Birth")]
        public int? DayOfBirth { get; set; }

        public string Gender { get; set; }

        public string Citizenship { get; set; }

        public RsaIdViewModel(int id, string idNumber, DateTime dateOfBirth, GenderEnum gender, string citizenship)
        {
            Id = id;
            IdNumber = idNumber;
            YearOfBirth = dateOfBirth.Year;
            MonthOfBirth = dateOfBirth.Month;
            DayOfBirth = dateOfBirth.Day;
            Gender = gender.ToString();
            Citizenship = citizenship;
        }
    }
}