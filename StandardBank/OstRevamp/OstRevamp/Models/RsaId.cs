﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace OstRevamp.Models
{
    public class RsaId
    {
        [Key]
        public int Id { get; set; }

        [Index]
        [Required]
        [MinLength(13),MaxLength(13)]
        [StringLength(13, ErrorMessage = "The {0} must be at {2} characters long.", MinimumLength = 13)]
        [DataType(DataType.Text)]
        [Display(Name = "Id Number")]
        public string IdNumber { get; set; }
        
        [Display(Name = "Date of Birth")]
        public DateTime DateOfBirth { get; set; }
        
        public GenderEnum Gender { get; set; }
        
        public string Citizenship { get; set; }

    }
}