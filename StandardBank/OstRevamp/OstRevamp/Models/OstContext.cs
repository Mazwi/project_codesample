﻿using System.Data.Entity;

namespace OstRevamp.Models
{
    public class OstContext : DbContext
    {
        public OstContext()
            : base("name=DefaultConnection")
        {
        }

        public DbSet<RsaId> RsaIds { get; set; }
    }
}