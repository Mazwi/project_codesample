﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Web.Http;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using OstRevamp;
using OstRevamp.Controllers;
using OstRevamp.Models;
using OstRevamp.Services;

namespace OstRevamp.Tests
{
    [TestClass]
    public class RsaIdControllerTest
    {
        [TestMethod]
        public void CreateRsaId()
        {
            // Arrange
            var rsaIdService = new RsaIdService();
            var dateOfBirthMock = DateTime.Now.AddMonths(-2).Date;
            var uniqueDigitsMock = DateTime.Now.Millisecond.ToString().PadRight(7, '1');
            string idNumberMock = $"{dateOfBirthMock.ToString("yyMMdd")}{uniqueDigitsMock}";
            RsaIdViewModel model = new RsaIdViewModel(
                0,
                idNumberMock,
                dateOfBirthMock,
                GenderEnum.None,
                string.Empty);

            // Act
            rsaIdService.CreateRsaId(model);
            RsaIdViewModel rsaIdDetails = rsaIdService.GetByIdNumber(idNumberMock);

            // Assert
            Assert.IsNotNull(rsaIdDetails, "RSA ID Could not be inserted and recalled successfully");
            Assert.AreEqual(rsaIdDetails.IdNumber, idNumberMock);
        }
        
        [TestMethod]
        public void TestValidRsaId()
        {
            // Arrange
            RsaIdController controller = new RsaIdController();
            var dateOfBirthMock = DateTime.Now.AddMonths(-2).Date;
            var uniqueDigitsMock = DateTime.Now.Millisecond.ToString().PadRight(7, '1');
            var idNumberMock = $"{dateOfBirthMock.ToString("yyMMdd")}{uniqueDigitsMock}";
            
            // Act
            string responseMessage = controller.ValidateIdNumber(idNumberMock);

            // Assert
            Assert.AreEqual(responseMessage, "OK");
        }

        [TestMethod]
        public void TestRsaIdEmpty()
        {
            // Arrange
            RsaIdController controller = new RsaIdController();

            // Act
            string responseMessage = controller.ValidateIdNumber(string.Empty);

            // Assert
            Assert.AreEqual(responseMessage, "Id Number connot be blank");
        }
        
        [TestMethod]
        public void TestRsaIdInvalidLength()
        {
            // Arrange
            RsaIdController controller = new RsaIdController();
            var dateOfBirthMock = DateTime.Now.AddMonths(-2).Date;
            var uniqueDigitsMock = DateTime.Now.Millisecond.ToString().PadRight(8, '1'); //Exceeds Length
            var idNumberMock = $"{dateOfBirthMock.ToString("yyMMdd")}{uniqueDigitsMock}";

            // Act
            string responseMessage = controller.ValidateIdNumber(idNumberMock);

            // Assert
            Assert.AreEqual(responseMessage, "Id Number must be 13 characters long");
        }

        [TestMethod]
        public void TestRsaIdInvalidCharacters()
        {
            // Arrange
            RsaIdController controller = new RsaIdController();
            var dateOfBirthMock = DateTime.Now.AddMonths(-2).Date;
            var uniqueDigitsMock = DateTime.Now.Millisecond.ToString().PadRight(7, 'a'); //Put alphabets
            var idNumberMock = $"{dateOfBirthMock.ToString("yyMMdd")}{uniqueDigitsMock}";

            // Act
            string responseMessage = controller.ValidateIdNumber(idNumberMock);

            // Assert
            Assert.AreEqual(responseMessage, "Id Number must be numbers only");
        }

        [TestMethod]
        public void TestInvalidFormatRsaId()
        {
            // Arrange
            RsaIdController controller = new RsaIdController();
            var dateOfBirthMock = DateTime.Now.AddMonths(-2).Date;
            var uniqueDigitsMock = DateTime.Now.Millisecond.ToString().PadRight(7, '1');
            var idNumberMock = $"{dateOfBirthMock.ToString("yyMM")}40{uniqueDigitsMock}"; //Put 40 on deta "day"

            // Act
            string responseMessage = controller.ValidateIdNumber(idNumberMock);

            // Assert
            Assert.AreEqual(responseMessage, "Invalid Id Number");
        }

        [TestMethod]
        public void TestIdAlreadyExist()
        {
            // Arrange
            RsaIdController controller = new RsaIdController();
            string idNumber = "1704196171111";

            // Act
            string responseMessage = controller.ValidateIdNumber(idNumber);

            // Assert
            Assert.AreEqual(responseMessage, "Id Number already registered");
        }
    }
}
