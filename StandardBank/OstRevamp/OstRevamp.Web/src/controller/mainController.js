angular.module('angularApp')
    .controller('mainController', [ '$scope', 'mainFactory', 'mainService', 'mainProvider', function ($scope, mainFactory, mainService, mainProvider) {
        
        $scope.getFactory  = mainFactory.getPrivate();
        $scope.getProvider = mainProvider.getPrivate();

        $scope.rsaId = {
            Id: 0,
            IdNumber: '',
            YearOfBirth: null,
            MonthOfBirth: null,
            DayOfBirth: null,
            Gender: -1,
            Citizenship: ''
        }
        
        $scope.state = {    
            submit: 'SUBMIT',
            valid: false,
            idErrorMessage: '',
            dateOfBirthErrorMessage: '',
            processing: false,
            showMessage: false,
            submitError: false,
            message: '',
        };

        $scope.validate = function(){
            $scope.idErrorMessage = '';
            if($scope.rsaId.IdNumber === '') {
                $scope.state = { 
                    submit: 'SUBMIT',
                    processing: false, 
                    idErrorMessage: 'Id Number is required'
                }
                return false;
            }

            $scope.rsaId.IdNumber = $scope.rsaId.IdNumber.replace(/ /g,'')
            if($scope.rsaId.IdNumber.length !== 13 || isNaN($scope.rsaId.IdNumber)) {
                $scope.state = { 
                    submit: 'SUBMIT',
                    processing: false, 
                    idErrorMessage: 'Id Number should be 13 numeric digits only'
                }
                return false;
            }

            if($scope.rsaId.IdNumber.lenght < '13' || $scope.rsaId.IdNumber.lenght < '13') {
                $scope.state = { 
                    submit: 'SUBMIT',
                    processing: false, 
                    idErrorMessage: 'Id Number should be 18 characters'
                }
                return false;
            }

            var dateOfBirthInput = new Date($scope.rsaId.YearOfBirth + '-' + $scope.rsaId.MonthOfBirth + '-' + $scope.rsaId.DayOfBirth);
            if(dateOfBirthInput == "Invalid Date"){
                $scope.state = { 
                    submit: 'SUBMIT',
                    processing: false, 
                    idErrorMessage: 'Invalid Date of Birth'
                }
                return false;
            }

            return true;
        }

        $scope.idInputChange = function(){
            var idNumberInput =  $scope.rsaId.IdNumber;
            if(idNumberInput.length > 1) {
                $scope.rsaId.YearOfBirth = parseInt(19 + idNumberInput.substr(0, 2));
            }
            if(idNumberInput.length > 3) {
                $scope.rsaId.MonthOfBirth = parseInt(idNumberInput.substr(2, 2));
            }
            if(idNumberInput.length > 5) {
                $scope.rsaId.DayOfBirth = parseInt(idNumberInput.substr(4, 2));
            }
        } 

        $scope.submitHandler = function(){
            $scope.idDetails = '';
            $scope.state = {    
                submit: 'Processing ...',
                processing: true
            };

            if(!$scope.validate()) return;

            mainService.createRsaId($scope.rsaId, function(response){
                //Listen to on Success Event
                if(response.status === 200) {
                    $scope.idDetails = response.data;
                    $scope.state = { 
                        submit: 'SUBMIT',
                        processing: false,   
                        showMessage: true,
                        submitError: false,
                        message: 'ID Number Successfully Added!',
                        idErrorMessage: ''
                    };
                } 
                else {
                    $scope.state = { 
                        submit: 'SUBMIT',
                        processing: false,   
                        showMessage: true,
                        submitError: true,
                        message: 'Unkwon Error!',
                        idErrorMessage: ''
                    };
                }

            }, function(response){
                //Listem to on Error Event
                console.log(response);
                if(response.status === 406) {
                    $scope.state = { 
                        submit: 'SUBMIT',
                        processing: false,   
                        showMessage: true,
                        submitError: true,
                        message: response.data,
                        idErrorMessage: ''
                    };
                } 
                else {
                    $scope.state = { 
                        submit: 'SUBMIT',
                        processing: false,   
                        showMessage: true,
                        submitError: true,
                        message: 'Error, please try again or contact admin',
                        idErrorMessage: ''
                    };  
                }           
            });
        }

        $scope.idNumberChangeHandler = function(){            
        }
        
    }]);