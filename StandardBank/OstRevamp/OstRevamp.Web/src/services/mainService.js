angular.module('angularApp')
    .service('mainService', ['$http', function ($http) {
        var config = {
            headers : {
                'Accept' : 'application/json',
                'Content-Type': 'application/json' //x-www-form-urlencoded;charset=utf-8;'
            }
        }
        this.createRsaId = function(rsaId, onSuccessCallback, onErrorCallback)  {
            var data = JSON.stringify(rsaId);   
            return $http.post('http://localhost:62597/api/RsaId', rsaId, config)
            .then(function(data, status, headers, config){
                onSuccessCallback(data);
            }, function(data){
                onErrorCallback(data);
            });
        };
    }]);