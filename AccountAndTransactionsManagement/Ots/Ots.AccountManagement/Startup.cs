﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Ots.AccountManagement.Startup))]
namespace Ots.AccountManagement
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
