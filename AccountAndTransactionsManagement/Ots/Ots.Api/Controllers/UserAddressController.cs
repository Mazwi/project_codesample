﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Web.Http;
using System.Web.Http.Description;
using Microsoft.AspNet.Identity;
using Ots.Api.Models;

namespace Ots.Api.Controllers
{
    [Authorize]
    public class UserAddressController : ApiController
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // POST: api/UserAddress
        [ResponseType(typeof(UserAddress))]
        public List<UserAddressViewModel> GetUserAddress()
        {
            var userId = User.Identity.GetUserId();
            List<UserAddress> userAddresses = db.UserAddresses.Include(c => c.Address).Where(a => a.UserId == userId).ToList();
            var userAddressesViewModel = new List<UserAddressViewModel>();

            foreach (var userAddress in userAddresses)
            {
                var userAddressViewModel = new UserAddressViewModel();
                var addressViewModel = new AddressViewModel();

                addressViewModel.AddressId = userAddress.Address.Id;
                addressViewModel.StreetAddress = userAddress.Address.StreetAddress;
                addressViewModel.Suburb = userAddress.Address.Suburb;
                addressViewModel.City = userAddress.Address.City;
                addressViewModel.Province = userAddress.Address.Province;
                addressViewModel.AreaCode = userAddress.Address.AreaCode;
                addressViewModel.Country = userAddress.Address.Country;

                userAddressViewModel.UserAddressId = userAddress.Id;
                userAddressViewModel.Address = addressViewModel;
                userAddressesViewModel.Add(userAddressViewModel);
            }

            return userAddressesViewModel;
        }

        // POST: api/UserAddress
        [ResponseType(typeof(UserAddress))]
        public IHttpActionResult Update(UserAddressViewModel userAddressViewModel)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var address = new Address();
            address.Id = userAddressViewModel.Address.AddressId;
            address.StreetAddress = userAddressViewModel.Address.StreetAddress;
            address.Suburb = userAddressViewModel.Address.Suburb;
            address.City = userAddressViewModel.Address.City;
            address.Province = userAddressViewModel.Address.Province;
            address.AreaCode = userAddressViewModel.Address.AreaCode;
            address.Country = userAddressViewModel.Address.Country;

            var user = new ApplicationUser();
            var username = User.Identity.Name;
            user = db.Users.Single(u => u.UserName == username);

            var userAddress = new UserAddress();
            userAddress.Id = userAddressViewModel.UserAddressId;
            userAddress.User = user;
            userAddress.UserId = user.Id;
            userAddress.Address = address;
            userAddress.AddressId = address.Id;

            db.Addresses.AddOrUpdate(userAddress.Address);
            db.UserAddresses.AddOrUpdate(userAddress);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = address.Id }, address);
        }
    }
}
