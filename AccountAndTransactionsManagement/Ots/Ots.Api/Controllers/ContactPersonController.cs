﻿using System.Data.Entity.Migrations;
using System.Linq;
using System.Web.Http;
using System.Web.Http.Description;
using Microsoft.AspNet.Identity;
using Ots.Api.Models;

namespace Ots.Api.Controllers
{
    [Authorize]
    public class ContactPersonController : ApiController
    {
        private ApplicationDbContext db = new ApplicationDbContext();
        // POST: api/ContactPerson
        [ResponseType(typeof(ContactPerson))]
        public ContactPersonViewModel GetContactPerson()
        {
            var userId = User.Identity.GetUserId();
            ContactPerson contactPerson = db.ContactPersons.SingleOrDefault(a => a.UserId == userId);

            var contactPersonViewModel = new ContactPersonViewModel();
            if (contactPerson == null) return null;

            contactPersonViewModel.ContactPersonId = contactPerson.Id;
            contactPersonViewModel.Name = contactPerson.Name;
            contactPersonViewModel.Telephone = contactPerson.Telephone;
            contactPersonViewModel.Cellphone = contactPerson.Cellphone;
            
            return contactPersonViewModel;
        }

        // POST: api/UserAddresses
        [ResponseType(typeof(ContactPerson))]
        public IHttpActionResult Update(ContactPersonViewModel contactPersonViewModel)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var user = new ApplicationUser();
            var username = User.Identity.Name;
            user = db.Users.Single(u => u.UserName == username);

            var contactPerson = new ContactPerson();
            contactPerson.Id = contactPersonViewModel.ContactPersonId;
            contactPerson.Name = contactPersonViewModel.Name;
            contactPerson.Telephone = contactPersonViewModel.Telephone;
            contactPerson.Cellphone = contactPersonViewModel.Cellphone;
            contactPerson.UserId = user.Id;
            contactPerson.User = user;
            
            db.ContactPersons.AddOrUpdate(contactPerson);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = contactPerson.Id }, contactPerson);
        }
    }
}
