﻿using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Net;
using System.Web.Http;
using System.Web.Http.Description;
using Ots.Api.Models;

namespace Ots.Api.Controllers
{
    public class AddressesController : ApiController
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: api/Addresses
        public IQueryable<Address> GetUserAddresses()
        {
            return db.Addresses;
        }

        // POST: api/Addresses
        [ResponseType(typeof(Address))]
        [Authorize]
        public IHttpActionResult Update(AddressViewModel addressViewModel)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var address = new Address();
            address.StreetAddress = addressViewModel.StreetAddress;
            address.Suburb = addressViewModel.Suburb;
            address.City = addressViewModel.City;
            address.Province = addressViewModel.Province;
            address.AreaCode = addressViewModel.AreaCode;
            address.Country = addressViewModel.Country;

            var user = new ApplicationUser();
            var username = User.Identity.Name;
            user = db.Users.Single(u => u.UserName == username);

            var userAddress = new UserAddress();
            userAddress.User = user;
            userAddress.Address = address;

            db.UserAddresses.AddOrUpdate(userAddress);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = address.Id }, address);
        }

        // GET: api/Addresses
        public IQueryable<Address> GetAddresses()
        {
            return db.Addresses;
        }

        // GET: api/Addresses/5
        [ResponseType(typeof(Address))]
        public IHttpActionResult GetAddress(int id)
        {
            Address address = db.Addresses.Find(id);
            if (address == null)
            {
                return NotFound();
            }

            return Ok(address);
        }

        // PUT: api/Addresses/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutAddress(int id, Address address)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != address.Id)
            {
                return BadRequest();
            }

            db.Entry(address).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!AddressExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Addresses
        //[ResponseType(typeof(Address))]
        //public IHttpActionResult PostAddress(Address address)
        //{
        //    if (!ModelState.IsValid)
        //    {
        //        return BadRequest(ModelState);
        //    }

        //    db.Addresses.Add(address);
        //    db.SaveChanges();

        //    return CreatedAtRoute("DefaultApi", new { id = address.Id }, address);
        //}

        // DELETE: api/Addresses/5
        [ResponseType(typeof(Address))]
        public IHttpActionResult DeleteAddress(int id)
        {
            Address address = db.Addresses.Find(id);
            if (address == null)
            {
                return NotFound();
            }

            db.Addresses.Remove(address);
            db.SaveChanges();

            return Ok(address);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool AddressExists(int id)
        {
            return db.Addresses.Count(e => e.Id == id) > 0;
        }
    }
}