﻿using System.Web.Http;
using Microsoft.AspNet.Identity;
using Ots.Application.Services;
using Ots.Application.DTOs;
using System.Collections.Generic;
using System.Linq;
using Ots.Api.Models;

namespace Ots.Api.Controllers
{
    [Authorize]
    public class TransactionController : ApiController
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // POST: api/Transaction
        public IHttpActionResult Capture(TransactionDto transaction)
        {
            var userId = User.Identity.GetUserId();
            transaction.CreatedBy = userId;
            transaction.Buyer = userId;
            //For Testing only, delete after email intergration
            transaction.Seller = db.Users.Single(u => u.UserName == transaction.Seller).Id;
            TransactionService.Init().CreateNewTransaction(transaction);

            return Ok();
        }

        // GET: api/Transaction
        public List<TransactionDto> GetTransactions()
        {
            var userId = User.Identity.GetUserId();
            var transactions = TransactionService.Init().GetUserTransactions(userId).ToList();

            //Replace User Ids with usernames
            for (int i = 0; i < transactions.Count; i++)
            {
                var transaction = transactions[i];
                transaction.Buyer = GetUserName(transaction.Buyer);
                transaction.Seller = GetUserName(transaction.Seller);

                transactions[i] = transaction;
            }
            
            return transactions;
        }

        private string GetUserName(string userId)
        {
            return db.Users.Single(u => u.Id == userId).UserName;
        }

        // PUT: api/Transaction/2
        [HttpPut]
        public IHttpActionResult UpdateStatus(int id, UpdateTransactionStatusDto transactionStatus)
        {
            transactionStatus.UserId = User.Identity.GetUserId();

            TransactionService.Init().UpdateTransactionStatus(id, transactionStatus);

            return Ok();
        }
    }
}
