﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Ots.Api.Models
{
    public class AddressViewModel
    {
        public int AddressId { get; set; }
        public string StreetAddress { get; set; }
        public string Suburb { get; set; }
        public string City { get; set; }
        public string Province { get; set; }
        public string AreaCode { get; set; }
        public string Country { get; set; }
    }
}