﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Ots.Api.Models
{
    public class UserAddress
    {
        [Key]
        public int Id { get; set; }
        public string UserId { get; set; }
        public int AddressId { get; set; }

        [ForeignKey("UserId")]
        public virtual ApplicationUser User { get; set; }

        [ForeignKey("AddressId")]
        public virtual Address Address { get; set; }
    }
}