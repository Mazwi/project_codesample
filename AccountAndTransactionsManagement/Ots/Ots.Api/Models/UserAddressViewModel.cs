﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Ots.Api.Models
{
    public class UserAddressViewModel
    {
        public int UserAddressId { get; set; }
        public string UserName { get; set; }
        public int AddressId { get; set; }
        public AddressViewModel Address { get; set; } 
    }
}