﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Ots.Api.Models
{
    public class ContactPersonViewModel
    {
        public int ContactPersonId { get; set; }
        public string Name { get; set; }
        public string Telephone { get; set; }
        public string Cellphone { get; set; }
        
        public string UserName { get; set; }
    }
}