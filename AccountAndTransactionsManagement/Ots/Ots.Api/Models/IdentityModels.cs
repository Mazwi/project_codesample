﻿using System.Security.Claims;
using Microsoft.AspNet.Identity.EntityFramework;
using System.Data.Entity;
using Ots.Infra.MySqlDB;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;

namespace Ots.Api.Models
{
    /////// User Original File to switch back to Sql Server ///////

    // You can add profile data for the user by adding more properties to your ApplicationUser
    // class, please visit http://go.microsoft.com/fwlink/?LinkID=317594 to learn more.
    public class ApplicationUser : IdentityUser
    {
        public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<ApplicationUser> manager, string authenticationType)
        {
            // Note the authenticationType must match the one defined in CookieAuthenticationOptions.AuthenticationType
            var userIdentity = await manager.CreateIdentityAsync(this, authenticationType);
            // Add custom user claims here
            return userIdentity;
        }


        public string FirstName { get; set; }
        public string Surname { get; set; }
    }


    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        static ApplicationDbContext()
        {
            Database.SetInitializer(new MySqlInitializer());
        }

        public ApplicationDbContext()
          : base("DefaultConnection")
        {
        }

        public static ApplicationDbContext Create()
        {
            return new ApplicationDbContext();
        }

        public DbSet<Address> Addresses { get; set ; }
        public DbSet<UserAddress> UserAddresses { get; set; }
        public DbSet<ContactPerson> ContactPersons { get; set; }
    }
}