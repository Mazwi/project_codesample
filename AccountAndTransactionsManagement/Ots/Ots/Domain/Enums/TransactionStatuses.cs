﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ots.Domain.Enums
{
    public enum TransactionStatuses
    {
        Captured = 1,
        OfferSent = 2,
        OfferApproved = 3,
        Accepted = 4,
        Rejected = 5,
        NewOffer = 6,
        Paid = 7,
        PaymentConfirmed = 8,
        DeliveryPicked = 9,
        DeliveryNotFound = 10,
        InTransit = 11,
        Delivered = 12,
        GoodsAccepted = 13,
        PaymentSettled = 14,
        Closed = 25,
        Expired = 26
    }
}
