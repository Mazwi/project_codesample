﻿namespace Ots.Domain.Enums
{
    public enum FeeCategories
    {
        Unspecified = 0,
        TransactionRate = 1,
        DeliveryRate = 2,
        LightDeliveryRate = 3,
        SmallDeliveryRate = 4,
        MediumDeliveryRate = 5,
        LargeDeliveryRate = 6,
        Other = 99
    }
}