﻿using Ots.Domain.Entities;
using Ots.Infra.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ots.Domain.Services
{
    public class TransactionFeeService
    {
        private UnitOfWork _unitOfWork;

        private TransactionFeeService()
        {
            _unitOfWork = new UnitOfWork(new OtsDbContext());
        }

        public void AddFee(TransactionFee transactionFee)
        {
            _unitOfWork.TransactionFees.Add(transactionFee);
            _unitOfWork.Complete();
        }

        public TransactionFee GetServiceFee()
        {
            return _unitOfWork.TransactionFees.Find(f => f.Category == Enums.FeeCategories.TransactionRate).Single();
        }

        public IEnumerable<TransactionFee> GetServiceAndDeliveryFee()
        {
            return _unitOfWork.TransactionFees.Find(f => f.Category == Enums.FeeCategories.TransactionRate || f.Category == Enums.FeeCategories.DeliveryRate);
        }

        public static TransactionFeeService Init()
        {
            return new TransactionFeeService();
        }
    }
}
