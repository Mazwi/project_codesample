﻿using Ots.Domain.Entities;
using Ots.Infra.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ots.Domain.Services
{
    public class ProductOfferService
    {
        private UnitOfWork _unitOfWork;

        public ProductOfferService()
        {
            _unitOfWork = new UnitOfWork(new OtsDbContext());
        }

        public void AddOffer(ProductOffer productOffer)
        {
            _unitOfWork.ProductOffers.Add(productOffer);
        }
    }
}
