﻿using Ots.Domain.Entities;
using Ots.Infra.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ots.Domain.Services
{

    public class ProductCategoryService
    {
        private UnitOfWork _unitOfWork;

        public ProductCategoryService()
        {
            _unitOfWork = new UnitOfWork(new OtsDbContext());
        }

        public void AddCategory(ProductCategory productCategory)
        {
            _unitOfWork.ProductCategories.Add(productCategory);
            _unitOfWork.Complete();
        }

        public void GetCategories()
        {
            _unitOfWork.ProductCategories.GetAll();
        }
    }
}
