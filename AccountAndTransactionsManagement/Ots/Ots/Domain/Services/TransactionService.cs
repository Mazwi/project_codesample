﻿using Ots.Infra.Repositories;
using System;
using System.Collections.Generic;
using Ots.Domain.Entities;
using Ots.Domain.Enums;

namespace Ots.Domain.Services
{

    public class TransactionService
    {
        private readonly UnitOfWork _unitOfWork;

        private TransactionService()
        {
            _unitOfWork = new UnitOfWork(new OtsDbContext());
        }

        private string GerenateReference()
        {
            return $"{DateTime.Now.ToString("yyMMddHHmmss")}-{DateTime.Now.Millisecond}";
        }

        public void AddTransaction(Transaction transaction, string sellerEmail, string sellerMobileNumber)
        {
            transaction.Reference = GerenateReference();
            _unitOfWork.Transactions.AddOrUpdate(transaction);
            _unitOfWork.Complete();
        }

        public IEnumerable<Transaction> GetUserTrancations(string userId)
        {
            var transactions = _unitOfWork.Transactions.GetUserTransactions(userId);

            return transactions;
        }

        public void UpdateTransactionStatus(int transactionId, TransactionStatuses status, string updatedBy, string comment)
        {
            var transaction = _unitOfWork.Transactions.Get(transactionId);
            transaction.Status = status;
            transaction.LastUpdateDate = DateTime.Now;

            //Todo: Optimise avoid double commit "_unitOfWork.Complete();"
            if (string.IsNullOrEmpty(comment))
            {
                _unitOfWork.Complete();
            }
            else
            {
                TransactionCommentService.Init(_unitOfWork).AddComment(transactionId, transaction.Status, comment, updatedBy);
            }
        }

        public static TransactionService Init()
        {
            return new TransactionService();
        }

    }
}
