﻿using Ots.Domain.Entities;
using Ots.Infra.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ots.Domain.Services
{
    public class ProductService
    {
        private UnitOfWork _unitOfWork;

        public ProductService()
        {
            _unitOfWork = new UnitOfWork(new OtsDbContext());
        }

        public IEnumerable<Product> GetTransactionProducts(int transactionId)
        {
            var transaction = _unitOfWork.Transactions.Get(transactionId);
            var products = _unitOfWork.Products.GetTransactionProductWithOffers(transaction);

            return products;
        }

        public void UpdateAcceptedOffer(int productId, int offerId)
        {
            var product = _unitOfWork.Products.Get(productId);
            var productOffer = _unitOfWork.ProductOffers.Get(offerId);
            product.AcceptedOffer = productOffer;
            _unitOfWork.Complete();
        }
    }
}
