﻿using Ots.Domain.Entities;
using Ots.Domain.Enums;
using Ots.Infra.Repositories;
using System;

namespace Ots.Domain.Services
{
    public class TransactionCommentService
    {
        private static UnitOfWork _unitOfWork;

        private TransactionCommentService()
        {
            //_unitOfWork = new UnitOfWork(new OtsDbContext());
        }

        public void AddComment(int transactionId, TransactionStatuses status, string comment, string updatedBy)
        {
            var transactionComment = new TransactionComment();
            transactionComment.Comment = comment;
            transactionComment.TransactionStatus = status;
            transactionComment.CapturedBy = updatedBy;
            transactionComment.CapturedDate = DateTime.Now;

            var tranasction = _unitOfWork.Transactions.Get(transactionId);
            transactionComment.Transaction = tranasction;

            _unitOfWork.TransactionComments.Add(transactionComment);
       
            _unitOfWork.Complete();
        }

        public static TransactionCommentService Init(UnitOfWork unitOfWork = null)
        {
            _unitOfWork = (unitOfWork == null) ? new UnitOfWork(new OtsDbContext()) : unitOfWork;
            return new TransactionCommentService();
        }
    }
}
