﻿using System;
using Ots.Domain.Enums;

namespace Ots.Domain.Entities
{
    public class TransactionComment
    {
        public int Id { get; set; }

        public TransactionStatuses TransactionStatus { get; set; }

        public string Comment { get; set; }

        public string CapturedBy { get; set; }

        public DateTime CapturedDate { get; set; }


        public virtual Transaction Transaction { get; set; }
    }
}
