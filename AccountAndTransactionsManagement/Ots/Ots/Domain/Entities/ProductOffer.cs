﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Ots.Domain.Entities
{
    public class ProductOffer
    {
        [Key]
        public int Id { get; set; }

        public decimal Amount { get; set; }

        public string Comment { get; set; }

        public DateTime OfferDate { get; set; }
    }
}