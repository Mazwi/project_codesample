﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Ots.Domain.Entities
{
    public class Product
    {
        [Key]
        public int Id { get; set; }

        public string Description { get; set; }

        public string Source { get; set; }

        public string SourceId { get; set; }

        public string Picture { get; set; }
        
        public int Quantity { get; set; }

        public decimal Price { get; set; }

        public virtual ICollection<ProductOffer> Offers { get; set; }

        public ProductOffer AcceptedOffer { get; set; }


        public virtual Transaction Transaction { get; set; }
    }
}
