﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Ots.Domain.Enums;

namespace Ots.Domain.Entities
{
    public class Transaction
    {
        public Transaction()
        {
            Products = new List<Product>();
            Comments = new List<TransactionComment>();
        }

        [Key]
        public int Id { get; set; }

        public string Reference { get; set; }

        public string Source { get; set; }

        public string SourceId { get; set; }

        public string Buyer { get; set; }

        public string Seller { get; set; }
        
        public virtual ICollection<Product> Products { get; set; }

        public bool UserOtsDelivery { get; set; }

        public decimal TotalFees { get; set; }
                
        public decimal TotalPaid { get; set; } 
         
        public TransactionStatuses Status { get; set; }

        public virtual ICollection<TransactionComment> Comments { get; set; } 
        
        public string CreatedBy { get; set; }

        public DateTime CapturedDate { get; set; }

        public DateTime LastUpdateDate { get; set; }


    }
}
