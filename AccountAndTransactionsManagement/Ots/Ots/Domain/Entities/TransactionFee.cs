﻿using System;
using System.ComponentModel.DataAnnotations;
using Ots.Domain.Enums;

namespace Ots.Domain.Entities
{
    public class TransactionFee
    {
        [Key]
        public int Id { get; set; }

        public string Description { get; set; }

        public FeeCategories Category { get; set; }

        public string CapturedBy { get; set; }

        public DateTime CapturedDate { get; set; }

        public DateTime LastUpdated { get; set; }

        public decimal Rate { get; set; }
        
    }
}
