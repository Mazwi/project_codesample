﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Ots.Domain.Entities
{
    public class ProductCategory
    {
        [Key]
        public int Id { get; set; }

        public string Description { get; set; }

        public string Size { get; set; }

        public string CapturedBy { get; set; }

        public DateTime CapturedDate { get; set; }
    }
}