using Ots.Infra.MySqlDB;

namespace Ots.Domain
{
    using System.Data.Entity;
    using System.Data.Entity.ModelConfiguration.Conventions;

    public class OtsDbContext : DbContext
    {
        // Your context has been configured to use a 'ItissWebStore' connection string from your application's 
        // configuration file (App.config or Web.config). By default, this connection string targets the 
        // 'ITISSWebApp.Api.Infra.ItissWebStore' database on your LocalDb instance. 
        // 
        // If you wish to target a different database and/or database provider, modify the 'ItissWebStore' 
        // connection string in the application configuration file.
        
        static OtsDbContext()
        {
            Database.SetInitializer(new MySqlInitializer());
        }

        public OtsDbContext()
            : base("name=DefaultConnection")
        {
        }

        // Add a DbSet for each entity type that you want to include in your model. For more information 
        // on configuring and using a Code First model, see http://go.microsoft.com/fwlink/?LinkId=390109.


        public DbSet<Entities.Transaction> Transactions { get; set; }
        public DbSet<Entities.Product> Products { get; set; }
        public DbSet<Entities.ProductOffer> ProductOffers { get; set; }
        public DbSet<Entities.ProductCategory> ProductCategories { get; set; }
        public DbSet<Entities.TransactionComment> TransactionComments { get; set; }
        public DbSet<Entities.TransactionFee> TransactionFees { get; set; }

    }
    
}