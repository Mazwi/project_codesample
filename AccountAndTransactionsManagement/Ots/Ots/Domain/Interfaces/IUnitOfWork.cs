﻿using System;

namespace Ots.Domain.Interfaces
{
    public interface IUnitOfWork : IDisposable
    {
        ITransactionRepository Transactions { get; }
        int Complete();
    }
}