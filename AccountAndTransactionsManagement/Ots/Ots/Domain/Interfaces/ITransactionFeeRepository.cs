﻿using Ots.Domain.Entities;
using System.Collections.Generic;

namespace Ots.Domain.Interfaces
{
    public interface ITransactionFeeRepository : IRepository<TransactionFee>
    {
        TransactionFee GetTransactionRate();
    }
}
