﻿using Ots.Domain.Entities;
using System.Collections.Generic;

namespace Ots.Domain.Interfaces
{
    public interface ITransactionRepository : IRepository<Transaction>
    {
        IEnumerable<Transaction> GetUserTransactions(string userId, int pageIndex = 1, int pageSize = 10);
    }
}
