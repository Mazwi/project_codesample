﻿using Ots.Domain.Entities;
using System.Collections.Generic;

namespace Ots.Domain.Interfaces
{
    public interface IProductRepository : IRepository<Product>
    {
        IEnumerable<Product> GetTransactionProductWithOffers(Transaction transaction);
    }
}
