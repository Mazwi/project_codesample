﻿using Ots.Domain.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ots.Application.DTOs
{
    public class UpdateTransactionStatusDto
    {
        public TransactionStatuses Status { get; set; }
        
        public string Comment { get; set; }

        public string UserId { get; set; }
    }
}
