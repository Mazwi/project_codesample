﻿using System;
using Ots.Domain.Enums;

namespace Ots.Application.DTOs
{
    public class CommentDto
    {
        public TransactionStatuses TransactionStatus { get; set; }

        public string StatusText { get; set; }

        public string Comment { get; set; }

        public string CapturedBy { get; set; }

        public DateTime CapturedDate { get; set; }
    }
}
