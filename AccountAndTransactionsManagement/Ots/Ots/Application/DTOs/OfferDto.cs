﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ots.Application.DTOs
{
    public class OfferDto
    {
        public int Id { get; set; }

        public decimal Amount { get; set; }

        public bool Accepted { get; set; }

        public string Comment { get; set; }

        public DateTime OfferDate { get; set; }
    }
}
