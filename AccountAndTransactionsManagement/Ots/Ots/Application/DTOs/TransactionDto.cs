﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using Ots.Domain.Entities;
using Ots.Domain.Enums;

namespace Ots.Application.DTOs
{
    public class TransactionDto
    {
        public int Id { get; set; }

        public string Reference { get; set; }

        public string Source { get; set; }

        public string SourceId { get; set; }

        public string Buyer { get; set; }

        public string Seller { get; set; }

        public string PersonEmail { get; set; }

        public string PersonMobileNumber { get; set; }

        public ICollection<ProductDto> Products { get; set; }

        public bool UseOtsDelivery { get; set; }

        public decimal TotalFees { get; set; }
        
        public decimal TotalPayable { get; set; }

        public TransactionStatuses Status { get; set; }

        public string StatusText { get; set; }

        public ICollection<FeeDto> Fees { get; set; }

        public ICollection<CommentDto> Comments { get; set; }

        public string CreatedBy { get; set; }

        public DateTime CapturedDate { get; set; }

        public DateTime LastUpdateDate { get; set; }

        public Transaction ToEntity(TransactionDto dto)
        {
            var transaction = new Transaction();
            transaction.Id = 0;
            transaction.Source = dto.Source;
            transaction.SourceId = dto.SourceId;
            transaction.Buyer = dto.Buyer;
            //For demo only, please revise (on Notification Module)
            transaction.Seller = dto.PersonEmail;
            transaction.UserOtsDelivery = dto.UseOtsDelivery;

            var products = new List<Product>();
            foreach (var item in dto.Products)
            {
                var product = new Product();
                product.Description = item.Description;
                product.Source = item.Source;
                product.SourceId = item.SourceId;
                product.Picture = item.Picture;
                product.Quantity = item.Quantity;
                product.Price = item.Price;

                var offer = new ProductOffer();
                offer.Id = 0;
                offer.Amount = item.Offers[0].Amount;
                offer.Comment = item.Offers[0].Comment;
                offer.OfferDate = DateTime.Now;

                var offers = new List<ProductOffer>();
                offers.Add(offer);
                product.Offers = offers;
                products.Add(product);
            }

            transaction.Products = products;
            transaction.Status = TransactionStatuses.Captured;
            transaction.CreatedBy = dto.CreatedBy;
            transaction.CapturedDate = DateTime.Now;
            transaction.LastUpdateDate = DateTime.Now;

            return transaction;
        }

        public List<TransactionDto> FromEntity(IEnumerable<Transaction> transactions)
        {

            var fees = Domain.Services.TransactionFeeService.Init().GetServiceAndDeliveryFee();

            var transactionsDto = new List<TransactionDto>();
            foreach (var transaction in transactions)
            {
                var transactionDto = new TransactionDto();
                transactionDto.Id = transaction.Id;
                transactionDto.Reference = transaction.Reference;
                transactionDto.Source = transaction.Source;
                transactionDto.SourceId = transaction.SourceId;
                transactionDto.Buyer = transaction.Buyer;
                transactionDto.Seller = transaction.Seller;

                var productsDto = new List<ProductDto>();
                foreach (var product in transaction.Products)
                {
                    var productDto = new ProductDto();
                    productDto.Description = product.Description;
                    productDto.Source = product.Source;
                    productDto.SourceId = product.SourceId;
                    productDto.Picture = product.Picture;
                    //productDto.CategoryId = product.CategoryId;
                    productDto.Price = product.Price;
                    productDto.Quantity = product.Quantity;

                    var offersDto = new List<OfferDto>();
                    foreach (var offer in product.Offers)
                    {
                        var offerDto = new OfferDto();
                        offerDto.Id = offer.Id;
                        offerDto.Amount = offer.Amount;
                        offerDto.Accepted = product.AcceptedOffer != null && product.AcceptedOffer.Amount == offer.Amount;
                        offerDto.Comment = offer.Comment;
                        offerDto.OfferDate = offer.OfferDate;

                        offersDto.Add(offerDto);
                    }

                    productDto.Offers = offersDto;
                    productsDto.Add(productDto);
                }

                transactionDto.Products = productsDto;
                transactionDto.UseOtsDelivery = transaction.UserOtsDelivery;

                var feesDto = new List<FeeDto>();
                if (transaction.UserOtsDelivery)
                {
                    foreach(var fee in fees)
                    {
                        var feeDto = new FeeDto();
                        feeDto.Description = fee.Description;
                        feeDto.Category = fee.Category;
                        feeDto.Rate = fee.Rate;
                        feesDto.Add(feeDto);
                    }
                }
                else
                {
                    var fee = fees.Where(f => f.Category == FeeCategories.TransactionRate).Single();
                    var feeDto = new FeeDto();
                    feeDto.Description = fee.Description;
                    feeDto.Category = fee.Category;
                    feeDto.Rate = fee.Rate;
                    feesDto.Add(feeDto);
                }

                transactionDto.Fees = feesDto;
                transactionDto.TotalFees = transaction.TotalFees;                
                transactionDto.TotalPayable = transaction.TotalPaid != 0 ? transaction.TotalPaid : productsDto.Sum(p => p.Offers[0].Amount * p.Quantity);
                transactionDto.Status = transaction.Status;
                transactionDto.StatusText = transaction.Status.ToString();
                
                var commentsDto = new List<CommentDto>();
                foreach(var comment in transaction.Comments)
                {
                    var commentDto = new CommentDto();
                    commentDto.TransactionStatus = comment.TransactionStatus;
                    commentDto.StatusText = comment.TransactionStatus.ToString();
                    commentDto.Comment = comment.Comment;
                    commentDto.CapturedBy = comment.CapturedBy;
                    commentDto.CapturedDate = comment.CapturedDate;
                    commentsDto.Add(commentDto);
                }

                transactionDto.Comments = commentsDto;
                transactionDto.CreatedBy = transaction.CreatedBy;
                transactionDto.CapturedDate = transaction.CapturedDate;
                transactionDto.LastUpdateDate = transaction.LastUpdateDate;
                transactionsDto.Add(transactionDto);
            }

            return transactionsDto;
        }

        public static TransactionDto CreateTransactionDto()
        {
            return new TransactionDto();
        }
    }
}
