﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ots.Application.DTOs
{
    public class ProductDto
    {
        public int Id { get; set; }

        public string Description { get; set; }

        public string Source { get; set; }

        public string SourceId { get; set; }

        public string Picture { get; set; }

        public int CategoryId { get; set; }

        public int Quantity { get; set; }

        public decimal Price { get; set; }

        public List<OfferDto> Offers { get; set; }
        
    }
}
