﻿using Ots.Domain.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ots.Application.DTOs
{
    public class FeeDto
    {

        public string Description { get; set; }

        public FeeCategories Category { get; set; }
       
        public decimal Rate { get; set; }
    }
}
