﻿using Ots.Application.DTOs;
using Ots.Domain.Services;
using System.Collections.Generic;

namespace Ots.Application.Services
{
    public class TransactionService
    {
        public void CreateNewTransaction(TransactionDto transaction)
        {
            Domain.Services.TransactionService.Init().AddTransaction(
                TransactionDto.CreateTransactionDto().ToEntity(transaction), 
                transaction.PersonEmail, transaction.PersonMobileNumber);
        }

        public IEnumerable<TransactionDto> GetUserTransactions(string userId)
        {
            var transactions = Domain.Services.TransactionService.Init().GetUserTrancations(userId);
            
            return TransactionDto.CreateTransactionDto().FromEntity(transactions);
        }

        public void UpdateTransactionStatus(int transactionId, UpdateTransactionStatusDto transactionStatus)
        {
            Domain.Services.TransactionService.Init()
                 .UpdateTransactionStatus(transactionId, transactionStatus.Status, transactionStatus.UserId, transactionStatus.Comment);
        }

        public static TransactionService Init()
        {
            return new TransactionService();
        }
    }
}
