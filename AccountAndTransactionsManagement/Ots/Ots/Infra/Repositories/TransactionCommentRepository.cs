﻿using Ots.Domain;
using Ots.Domain.Entities;

namespace Ots.Infra.Repositories
{
    public class TransactionCommentRepository : Repository<TransactionComment>
    {
        public TransactionCommentRepository(OtsDbContext context) : base(context)
        {
        }
    }
}
