﻿using Ots.Domain;
using Ots.Domain.Entities;
using Ots.Domain.Interfaces;
using System.Collections.Generic;
using System.Linq;
using System.Data.Entity;

namespace Ots.Infra.Repositories
{
    public class ProductRepository : Repository<Product>, IProductRepository 
    {
        public ProductRepository(OtsDbContext context) : base(context)
        {
        }

        public IEnumerable<Product> GetTransactionProductWithOffers(Transaction transaction)
        {
            IEnumerable<Product> products = OtsDbContext.Products
                .Where(p => p.Transaction == transaction)
                .Include(p => p.Offers)
                .Include(p => p.AcceptedOffer)
                .ToList();

            return products;
        }

        public OtsDbContext OtsDbContext
        {
            get { return Context as OtsDbContext; }
        }
    }
}
