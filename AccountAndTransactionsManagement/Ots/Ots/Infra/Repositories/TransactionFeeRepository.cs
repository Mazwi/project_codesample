﻿using Ots.Domain;
using Ots.Domain.Entities;
using System.Collections.Generic;
using System.Linq;
using Ots.Domain.Interfaces;
using Ots.Domain.Enums;

namespace Ots.Infra.Repositories
{
    public class TransactionFeeRepository : Repository<TransactionFee>, ITransactionFeeRepository
    {
        public TransactionFeeRepository(OtsDbContext context) : base(context)
        {
        }

        public TransactionFee GetTransactionRate()
        {
            TransactionFee fee = OtsDbContext.TransactionFees
                .Single(f => f.Category == FeeCategories.TransactionRate);

            return fee;
        }
        public OtsDbContext OtsDbContext
        {
            get { return Context as OtsDbContext; }
        }
    }
}
