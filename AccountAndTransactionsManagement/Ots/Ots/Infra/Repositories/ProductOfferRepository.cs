﻿using Ots.Domain;
using Ots.Domain.Entities;

namespace Ots.Infra.Repositories
{
    public class ProductOfferRepository : Repository<ProductOffer>
    {
        public ProductOfferRepository(OtsDbContext context) : base(context)
        {
        }
    }
}
