﻿using Ots.Domain;
using Ots.Domain.Entities;

namespace Ots.Infra.Repositories
{
    public class ProductCategoryRepository : Repository<ProductCategory>
    {
        public ProductCategoryRepository(OtsDbContext context) : base(context)
        {
        }
    }
}
