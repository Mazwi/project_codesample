﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using Ots.Domain;
using Ots.Domain.Entities;
using Ots.Domain.Interfaces;

namespace Ots.Infra.Repositories
{
    public class TransactionRepository : Repository<Transaction>, ITransactionRepository
    {
        public TransactionRepository(OtsDbContext context) : base(context)
        {
        }

        public IEnumerable<Transaction> GetUserTransactions(string userId, int pageIndex = 1, int pageSize = 10)
        {
            IEnumerable<Transaction> transactions = OtsDbContext.Transactions
                .Where(t => t.Buyer == userId || t.Seller == userId)
                .Include(t => t.Products)
                .Include(t => t.Comments)
                .OrderBy(t => t.LastUpdateDate)
                .Skip((pageIndex - 1) * pageSize)
                .Take(pageSize)
                .ToList();

            return transactions;
        }
        public OtsDbContext OtsDbContext
        {
            get { return Context as OtsDbContext; }
        }
    }
}
