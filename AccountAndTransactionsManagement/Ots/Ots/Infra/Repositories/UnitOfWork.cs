﻿using Ots.Domain;
using Ots.Domain.Interfaces;

namespace Ots.Infra.Repositories
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly OtsDbContext _context;

        public ITransactionRepository Transactions { get; private set; }
        public IProductRepository Products { get; private set; }
        public ProductOfferRepository ProductOffers { get; private set; }
        public ProductCategoryRepository ProductCategories { get; private set; }
        public TransactionCommentRepository TransactionComments { get; set; }
        public ITransactionFeeRepository TransactionFees { get; set; }

        public UnitOfWork(OtsDbContext context)
        {
            _context = context;
            Transactions = new TransactionRepository(_context);
            Products = new ProductRepository(_context);
            ProductOffers = new ProductOfferRepository(_context);
            ProductCategories = new ProductCategoryRepository(_context);
            TransactionComments = new TransactionCommentRepository(_context);
            TransactionFees = new TransactionFeeRepository(_context);
        }

        public int Complete()
        {
            return _context.SaveChanges();
        }

        public void Dispose()
        {
            _context.Dispose();
        }
    }
}
