namespace Ots.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class RemoveRelationshipProductCategory : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("Products", "CategoryId", "ProductCategories");
            DropIndex("Products", new[] { "CategoryId" });
            DropColumn("Products", "CategoryId");
        }
        
        public override void Down()
        {
            AddColumn("Products", "CategoryId", c => c.Int(nullable: false));
            CreateIndex("Products", "CategoryId");
            AddForeignKey("Products", "CategoryId", "dbo.ProductCategories", "Id", cascadeDelete: true);
        }
    }
}
