namespace Ots.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class RenameTransactionFee_AmountToRate : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.TransactionFees", "Rate", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            DropColumn("dbo.TransactionFees", "Amount");
        }
        
        public override void Down()
        {
            AddColumn("dbo.TransactionFees", "Amount", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            DropColumn("dbo.TransactionFees", "Rate");
        }
    }
}
