namespace Ots.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class FixProductCategoryCaptureDateDataTypeToDateTime : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.ProductCategories", "CapturedDate", c => c.DateTime(nullable: false, precision: 0));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.ProductCategories", "CapturedDate", c => c.String(unicode: false));
        }
    }
}
