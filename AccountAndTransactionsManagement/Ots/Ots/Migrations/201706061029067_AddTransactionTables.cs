namespace Ots.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddTransactionTables : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.ProductCategories",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Description = c.String(unicode: false),
                        Size = c.String(unicode: false),
                        CapturedBy = c.String(unicode: false),
                        CapturedDate = c.String(unicode: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.ProductOffers",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Amount = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Comment = c.String(unicode: false),
                        OfferDate = c.DateTime(nullable: false, precision: 0),
                        Product_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Products", t => t.Product_Id)
                .Index(t => t.Product_Id);
            
            CreateTable(
                "dbo.Products",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Description = c.String(unicode: false),
                        Source = c.String(unicode: false),
                        SourceId = c.String(unicode: false),
                        Picture = c.String(unicode: false),
                        CategoryId = c.Int(nullable: false),
                        Quantity = c.Int(nullable: false),
                        Price = c.Decimal(nullable: false, precision: 18, scale: 2),
                        AcceptedOffer_Id = c.Int(),
                        Transaction_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.ProductOffers", t => t.AcceptedOffer_Id)
                .ForeignKey("dbo.ProductCategories", t => t.CategoryId, cascadeDelete: true)
                .ForeignKey("dbo.Transactions", t => t.Transaction_Id)
                .Index(t => t.CategoryId)
                .Index(t => t.AcceptedOffer_Id)
                .Index(t => t.Transaction_Id);
            
            CreateTable(
                "dbo.Transactions",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Reference = c.String(unicode: false),
                        Source = c.String(unicode: false),
                        SourceId = c.String(unicode: false),
                        Buyer = c.String(unicode: false),
                        Seller = c.String(unicode: false),
                        TotalFees = c.Decimal(nullable: false, precision: 18, scale: 2),
                        TotalPaid = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Status = c.Int(nullable: false),
                        CreatedBy = c.String(unicode: false),
                        CapturedDate = c.DateTime(nullable: false, precision: 0),
                        LastUpdateDate = c.DateTime(nullable: false, precision: 0),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.TransactionComments",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        TransactionStatus = c.Int(nullable: false),
                        Comment = c.String(unicode: false),
                        CapturedBy = c.String(unicode: false),
                        CapturedDate = c.DateTime(nullable: false, precision: 0),
                        Transaction_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Transactions", t => t.Transaction_Id)
                .Index(t => t.Transaction_Id);
            
            CreateTable(
                "dbo.TransactionFees",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Description = c.String(unicode: false),
                        Category = c.Int(nullable: false),
                        CapturedBy = c.String(unicode: false),
                        CapturedDate = c.DateTime(nullable: false, precision: 0),
                        LastUpdated = c.DateTime(nullable: false, precision: 0),
                        Amount = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Transaction_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Transactions", t => t.Transaction_Id)
                .Index(t => t.Transaction_Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Products", "Transaction_Id", "dbo.Transactions");
            DropForeignKey("dbo.TransactionFees", "Transaction_Id", "dbo.Transactions");
            DropForeignKey("dbo.TransactionComments", "Transaction_Id", "dbo.Transactions");
            DropForeignKey("dbo.ProductOffers", "Product_Id", "dbo.Products");
            DropForeignKey("dbo.Products", "CategoryId", "dbo.ProductCategories");
            DropForeignKey("dbo.Products", "AcceptedOffer_Id", "dbo.ProductOffers");
            DropIndex("dbo.TransactionFees", new[] { "Transaction_Id" });
            DropIndex("dbo.TransactionComments", new[] { "Transaction_Id" });
            DropIndex("dbo.Products", new[] { "Transaction_Id" });
            DropIndex("dbo.Products", new[] { "AcceptedOffer_Id" });
            DropIndex("dbo.Products", new[] { "CategoryId" });
            DropIndex("dbo.ProductOffers", new[] { "Product_Id" });
            DropTable("dbo.TransactionFees");
            DropTable("dbo.TransactionComments");
            DropTable("dbo.Transactions");
            DropTable("dbo.Products");
            DropTable("dbo.ProductOffers");
            DropTable("dbo.ProductCategories");
        }
    }
}
