namespace Ots.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddUseOtsDeliveryField : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Transactions", "UserOtsDelivery", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Transactions", "UserOtsDelivery");
        }
    }
}
