namespace Ots.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class RemoveRelationshipTransactionTransactionFees : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("TransactionFees", "Transaction_Id", "Transactions");
            DropIndex("TransactionFees", new[] { "Transaction_Id" });
            DropColumn("TransactionFees", "Transaction_Id");
        }
        
        public override void Down()
        {
            AddColumn("TransactionFees", "Transaction_Id", c => c.Int());
            CreateIndex("TransactionFees", "Transaction_Id");
            AddForeignKey("TransactionFees", "Transaction_Id", "Transactions", "Id");
        }
    }
}
