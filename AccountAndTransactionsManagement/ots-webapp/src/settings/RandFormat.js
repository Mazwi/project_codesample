import numeral from 'numeral';

var RandFormat = numeral.register('locale', 'eng', {
    delimiters: {
        thousands: ' ',
        decimal: '.'
    },
    abbreviations: {
        thousand: 'k',
        million: 'm',
        billion: 'b',
        trillion: 't'
    },
    ordinal : function (number) {
        return number === 1 ? 'er' : 'ème';
    },
    currency: {
        symbol: 'R'
    }
});

// switch between locales
numeral.locale('eng');

export default RandFormat;