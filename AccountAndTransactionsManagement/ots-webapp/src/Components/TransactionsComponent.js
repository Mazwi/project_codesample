import React, { Component } from 'react';
import {Table, 
		  TableBody,
		  TableHeader,
		  TableHeaderColumn,
		  TableRow,
		  TableRowColumn} from 'material-ui/Table';

import moment from 'moment';
import numeral from 'numeral';
import RandFormat from '../settings/RandFormat';

class TransactionItem extends Component {

	_referenceCellClickHandler = (transaction) => {
		this.props.onReferenceCellClick(transaction);
	}

	render() {

		return(<Table>
					<TableHeader displaySelectAll={false} adjustForCheckbox={false}>
						<TableRow>
							<TableHeaderColumn>Reference</TableHeaderColumn>
							<TableHeaderColumn>Date</TableHeaderColumn>
							<TableHeaderColumn>Buyer / Seller</TableHeaderColumn>
							<TableHeaderColumn>Amount R</TableHeaderColumn>								
							<TableHeaderColumn>Status</TableHeaderColumn>
						</TableRow>
					</TableHeader>
					<TableBody 
		            displayRowCheckbox={false}
		            deselectOnClickaway={true}
		            showRowHover={true} >
						{this.props.transactions.map((transaction, i) => (
							<TableRow>
								<TableRowColumn><a href="#" onClick={() => this._referenceCellClickHandler(transaction)}>{transaction.Reference}</a></TableRowColumn>
								<TableRowColumn>{moment(transaction.CapturedDate).format('MMM DD, YYYY HH:mm:ss')}</TableRowColumn>
								<TableRowColumn>{transaction.Buyer == this.props.currentUser ? transaction.Seller : transaction.Buyer}</TableRowColumn>
								<TableRowColumn>{numeral(transaction.TotalPayable).format('$0,0.00')}</TableRowColumn>
								<TableRowColumn>{transaction.StatusText}</TableRowColumn>
							</TableRow>
						))}					
					</TableBody>
				</Table>
		)
	}
}

export default TransactionItem