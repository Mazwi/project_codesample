import React, { Component } from 'react';
import Paper from 'material-ui/Paper';
import TextField from 'material-ui/TextField';
import RaisedButton from 'material-ui/RaisedButton';
import FlatButton from 'material-ui/FlatButton';
import SelectField from 'material-ui/SelectField';
import MenuItem from 'material-ui/MenuItem';
import Divider from 'material-ui/Divider';

import defaultPicture from '../images/shopping-bag.png';

import ProductModel from '../Models/ProductModel';

class AddProduct extends Component {
	constructor(props) {
    	super(props);
    	this.state = {
    		redirectToLogin: false,
    		source: props.sourceUrl,
	    	description: '',
	    	sourceId: '',
    		picture: defaultPicture,
    		category: '',
	      	price: ''
	      };
    }

	 _handleDescriptionChange = (e) => {
        this.setState({ description: e.target.value });
    }
	 _handleSourceChange = (e) => {
        this.setState({ source: e.target.value });
    }
	 _handleSourceIdChange = (e) => {
        this.setState({ sourceId: e.target.value });
    }
	 _handleCategoryChange = (event, index, value) => {
        this.setState({ category: value });
    }	
	 _handlePriceChange = (e) => {
        this.setState({ price: e.target.value });
    }

	_handleAddProduct = async () => {
		
		var product =  new ProductModel(
			this.state.source,
			this.state.description,
			this.state.sourceId,
			this.state.picture,
			this.state.category,
			this.state.category,
			this.state.price
		)
		this.props.addProductCallback(product);
	}

	render() {
    	return (  		
    			<div>
	    			<TextField hintText="Source Website" floatingLabelText="Source Website" style={{marginRight: 20}} value={this.state.source} onChange={this._handleSourceChange} />
	    			<TextField hintText="Source Product Id/Ref" floatingLabelText="Source Product Id/Ref" style={{marginRight: 20}} value={this.state.sourceId} onChange={this._handleSourceIdChange} /><br />
	    			<TextField hintText="Descirption" floatingLabelText="Descirption" style={{marginRight: 20}} value={this.state.description} onChange={this._handleDescriptionChange} />	    			
			        <TextField hintText="Price" floatingLabelText="Price" style={{marginRight: 20}} value={this.state.price} onChange={this._handlePriceChange} /><br />
			        <SelectField style={{display: "none"}}
			          floatingLabelText="Category"
			          value={this.state.category}
			          onChange={this._handleCategoryChange}>
				          <MenuItem value={0} primaryText="" />
				          <MenuItem value={1} primaryText="Light Item" />
				          <MenuItem value={2} primaryText="Small Item" />
				          <MenuItem value={3} primaryText="Medium Item" />
				          <MenuItem value={4} primaryText="Large Item" />
			        </SelectField><br /> 		    			
			        <FlatButton label="ADD" primary={true} style={{margin: 2}} onTouchTap={this._handleAddProduct} />
			        <FlatButton label="CANCEL" secondary={true} style={{margin: 2}} onTouchTap={this.props.cancelProductCallback} />
			        <br /><br /><br />
    			</div>
    	)
	}
}

export default AddProduct;