import React, { Component } from 'react';
import FlatButton from 'material-ui/FlatButton';
import { lightBlue600, grey200, grey600 } from 'material-ui/styles/colors';

import numeral from 'numeral';
import RandFormat from '../settings/RandFormat';

class DisplayProductItem extends Component {
	render() {
    	return (  		
			<div style={styles.container}>
	        	<img src={this.props.item.Picture} style={{width:60, marginRight: 20}} alt="" />
				<div style={{display: "inline-block", textAlign: "left"}}>
	    			<a style={{color: "gray"}} href={this.props.item.Source}>{this.props.item.Source}</a><br />
	    			<div style={{display: "inline-block", marginTop: 10}}>
	    				<span style={styles.Description}>{this.props.item.Description}</span> #
	    				<span>{this.props.item.SourceId}</span><br />
	    				<span style={{color: "maroon", fontSize: 15}}>{numeral(this.props.item.Price).format('$0,0.00')}</span>
	    			</div>
	    			<div style={styles.productDetails}>
	    				<div style={{display: "inline-block"}}>	 	    	
			        		<span>Quantity: {this.props.item.Quantity}</span>
		        		</div>
		        		<div style={styles.offerPanel}>
	    					<span style={{color: lightBlue600}}>Offer:  </span>
	    					<span>{numeral(this.props.item.Offers[this.props.item.Offers.length - 1].Amount).format('$0,0.00')}</span>
						</div><br />
	    				<span style={{fontSize: 12, color: grey600}}> = {numeral(this.props.item.Quantity * this.props.item.Offers[this.props.item.Offers.length - 1].Amount).format('$0,0.00')} (sub-total)</span>
					</div>
				</div>
			</div>
    	)
	}
}

const styles = {
	container: {
		backgroundColor: grey200,
		marginBottom: 10, 
		padding: 10,
		borderRadius: 5
	},
	description: {
		color: "darkgray", 
		fontWeight: "bold", 
		marginRight: 10
	},
	productDetails: {
		display: "inline-block", 
		textAlign: "center", 
		marginLeft: 50
	},
	offerPanel: {
		display: "inline-block", 
		textAlign: "center",
		marginLeft: 15		
	}
}

export default DisplayProductItem;