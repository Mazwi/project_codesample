import React, { Component } from 'react';
import FlatButton from 'material-ui/FlatButton';
import { lightBlue600 } from 'material-ui/styles/colors';

import numeral from 'numeral';
import RandFormat from '../settings/RandFormat';

class ProductComponent extends Component {
	constructor(props) {
    	super(props);
    	this.state = {
    		quantity: 1,
    		offer: props.item.Offers[0].Amount
	      };
    }

	_handleQtyChange = (e) => {
        this.setState({ quantity: e.target.value });
    }
	_incrementQty = () => {
	 	this.state.quantity++;
        this.setState({ quantity: this.state.quantity });
		this.props.qtyChangeCallback(this.props.itemIndex, this.state.quantity);
    }
	_decrementQty = () => {
	 	if(this.state.quantity !== 1) this.state.quantity--;
        this.setState({ quantity: this.state.quantity });
		this.props.qtyChangeCallback(this.props.itemIndex, this.state.quantity);
    }
    _handleQtyChange = (e) => {
        this.setState({ quantity: e.target.value });
		this.props.qtyChangeCallback(this.props.itemIndex, e.target.value);
    }
	_handleOfferChange = (e) => {
        this.setState({ offer: e.target.value });
		this.props.offerChangeCallback(this.props.itemIndex, e.target.value);
    }

	render() {
		//numeral.locale('eng');
    	return (  		
	    		<div style={{display: "inline-block"}}>
		        	<img src={this.props.item.Picture} style={{width:80, marginRight: 20}} alt="" />
	    			<div style={{display: "inline-block", textAlign: "left"}}>
		    			<a style={{color: "gray"}} href={this.props.item.Source}>{this.props.item.Source}</a><br />
		    			<div style={{display: "inline-block", marginTop: 10}}>
		    				<span style={styles.Description}>{this.props.item.Description}</span> #
		    				<span>{this.props.item.SourceId}</span><br />
		    				<span style={{fontSize: 14}}>{this.props.item.Category}</span><br />
		    				<span style={{color: "maroon", fontSize: 15}}>{numeral(this.props.item.Price).format('$0,0.00')}</span>
		    			</div>
		    			<div style={styles.productDetails}>
		    				<div style={{display: "inline-block"}}>	 	    	
				        		<FlatButton label="-" secondary={true} style={{borderStyle: "none"}} onTouchTap={this._decrementQty} />
				        		<input type="number" style={styles.qtyInput} value={this.state.quantity} onChange={this._handleQtyChange} />
				        		<FlatButton label="+" primary={true} style={{borderStyle: "none"}} onClick={this._incrementQty} />
			        		</div><br />
			        		<div style={styles.offerPanel}>
		    					<span style={{color: lightBlue600}}>offer:  </span>
		    					<input placeholder="Offer Amount" type="offer" style={styles.offerInput} value={this.state.offer} onChange={this._handleOfferChange} />
	    					</div>
	    				</div>
	    			</div>
	    		</div>
    	)
	}
}

const styles = {
	container: {
		display: "inline-block",
		width: "100%",
	},
	description: {
		color: "darkgray", 
		fontWeight: "bold", 
		marginRight: 10
	},
	productDetails: {
		display: "inline-block", 
		textAlign: "center", 
		marginLeft: 50
	},
 	qtyInput: {
		width: 50, 
		borderStyle: "none", 
		textAlign: "center"
	},
	offerPanel: {
		display: "inline-block", 
		textAlign: "center"
	},
	offerInput: {
		color: "darkgray", 
		marginRight: 0, 
		width: 140, 
		padding: 3,
		paddingLeft: 5, 
		borderStyle: "solid", 
		borderWidth: 0, 
		borderColor: "lightGray"
	}
}

export default ProductComponent;