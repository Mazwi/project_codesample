import React, { Component } from 'react';
import FlatButton from 'material-ui/FlatButton';

import IconMenu from 'material-ui/IconMenu';
import MenuItem from 'material-ui/MenuItem';
import IconButton from 'material-ui/IconButton';
import PersonIcon from 'material-ui/svg-icons/social/person';

class User extends Component {
  
  constructor(props) {
    super(props);
    
    this.state = {
      accessToken: '',
      redirectToLogin: false
    };    
  }

  componentDidMount() {    
    if(sessionStorage.accessToken !== undefined && sessionStorage.accessToken !== '') 
		{
			this.setState({ accessToken: JSON.parse(sessionStorage.accessToken).access_token }); 
		}

    var parent = this;
    window.addEventListener('loggedIn', function(event) {
      parent.setState({ accessToken: sessionStorage.accessToken });
    });
  }

	redirect = async (item) => {
    switch(item.key){
      case "1":        
        sessionStorage.page = "/Account";
        window.location = "/";        
        //var redirectPageEvent = new CustomEvent('RedirectPage', { detail: { page: "/Account" } });
        //window.dispatchEvent(redirectPageEvent);
        break; 
      case "2":
        sessionStorage.page = "/List";
        window.location = "/";
        break;
      case "3":
        sessionStorage.page = "/Capture";
        window.location = "/";
        break; 
      case "9":
        sessionStorage.page = "/Login";
        window.location = "/";
        sessionStorage.accessToken = '';
        this.setState({ accessToken: '' });
        break;
      default: 
        window.location = "/Login"; 
    }
  }

	render() {
		return(
      this.state.accessToken === undefined || this.state.accessToken === '' ? <FlatButton label="Login" primary={true} onTouchTap={ () => this.redirect({ key: 0 }) } /> :
			<IconMenu
        iconButtonElement={<IconButton><PersonIcon color="maroon" /></IconButton>}
        anchorOrigin={{horizontal: 'left', vertical: 'bottom'}}
        targetOrigin={{horizontal: 'left', vertical: 'bottom'}}
        onItemTouchTap={(e, item) => this.redirect(item)}>
        <MenuItem key={1} primaryText="My Account" />
        <MenuItem key={2} primaryText="My Transactions" />
        <MenuItem key={3} primaryText="Test" />
        <MenuItem key={9} primaryText="Sign out" />
      </IconMenu>
  	)
	}
}

export default User;