import React, { Component } from 'react';
import RaisedButton from 'material-ui/RaisedButton';
import FlatButton from 'material-ui/FlatButton';
import Dialog from 'material-ui/Dialog';
import SelectField from 'material-ui/SelectField';
import MenuItem from 'material-ui/MenuItem';
import TextField from 'material-ui/TextField';
import Divider from 'material-ui/Divider';

import TransactionStatusModel from '../Models/TransactionStatusModel';

class TransactionUpdate extends Component {
	state = {
		userName: '',
		actions: '',
		open: false,
		reason: '',
		comments: '',
		notValid: true
	}

	componentWillMount() {
		var currentUserName = JSON.parse(sessionStorage.accessToken).userName;

		const actionButtons = { 
			updateTransactionButtons: <div style={{ display: "inline-block" }}>
							<RaisedButton label="Accept" primary={true} style={{marginRight: 30}} onClick={this.props.acceptActionHandler} />
							<RaisedButton label="Reject" secondary={true} style={{marginRight: 30}} onClick={this._handleOpen} />
						</div>,
			payButtons: <RaisedButton label="Pay" primary={true} style={{marginRight: 30}} onClick={this.props.payActionHandler} />,
			paymentReceivedButtons: <RaisedButton label="Confirm Payment" primary={true} style={{marginRight: 30}} onClick={this.props.confirmPaymentActionHandler} />
		}

		if(this.props.seller === currentUserName && this.props.status < 3){
			this.setState({ userName: currentUserName, actions: actionButtons.updateTransactionButtons })
		}
		else if (this.props.seller !== currentUserName && this.props.status == 4) {
			this.setState({ userName: currentUserName, actions: actionButtons.payButtons })
		}
		else if (this.props.seller === currentUserName && this.props.status > 6) {
			this.setState({ userName: currentUserName, actions: actionButtons.paymentReceivedButtons })
		}
	}

	_handleOpen = () => {
    	this.setState({open: true});
	};

	_handleClose = () => {
		this.setState({open: false});
	};

	_handleReasonChange = (event, index, value) => {
		var isNotValid = value === '' || this.state.comments === '';
		this.setState({ reason: value, notValid: isNotValid })
	}

	_handleCommentChange = (e) => {
		var isNotValid = e.target.value === '' || this.state.reason === '';
		this.setState({ comments: e.target.value, notValid: isNotValid })
	}

	_handleRejectSubmit = () => {		
		var transactionStatus = new TransactionStatusModel(
			5, this.state.reason + ': ' + this.state.comments
		)
		this.setState({ open: false });
		this.props.rejectActionHandler(transactionStatus);
	}

	_handleBack = () => {
	 	this.props.history.push({
		     pathname: '/Details'
		 })
	}

	render(){

		const dialogActions = [
				  <FlatButton
				    label="Cancel"
				    primary={true}
				    onTouchTap={this._handleClose}
				  />,
				  <FlatButton
				    label="Submit"
				    secondary={true}
				    disabled={this.state.notValid}
				    onTouchTap={this._handleRejectSubmit}
				  />,
				]

		return(
				<div style={{paddingLeft: 20}}>
					<RaisedButton label="Back to List" default={true} style={{marginRight: 30}} onClick={this.props.backToListActionHangler} />
					{this.state.actions}
					<Dialog
			          title="Kindly provide the reason for Rejecting this Offer"
			          actions={dialogActions}
			          modal={true}
			          open={this.state.open}>			          
				        <SelectField
				          floatingLabelText="Reason"
				          value={this.state.reason}
				          onChange={this._handleReasonChange}>
				          <MenuItem value="Offer Amount To Little" primaryText="Offer Amount to Little" />
				          <MenuItem value="Item(s) no longer Available" primaryText="Item(s) no longer Available" />
				          <MenuItem value="I don't trust this" primaryText="I don't trust this" />
				        </SelectField><br />
				        <TextField
					      hintText="Comments / Further Explanation"
					      multiLine={true}
					      rows={2}
					      rowsMax={5}
					      value={this.state.comments}
					      onChange={this._handleCommentChange}
					    />
			        </Dialog>
				</div>
			)
	}
}

export default TransactionUpdate