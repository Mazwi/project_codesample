import React, { Component } from 'react';
import './App.css';
import logo from './images/Logo-placeholder.png';
import HeaderBackgound from './images/header-background-edited.jpg';
import {
  BrowserRouter as Router,
  Route,
  Switch,
  Redirect
} from 'react-router-dom'

import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import { grey700 } from 'material-ui/styles/colors';

import { RouteTransition } from 'react-router-transition';

import Notifications from 'react-notify-toast';

import User from './Components/UserComponent';

import Login from './Screens/Account/Login';
import Register from './Screens/Account/Register';
import Address from './Screens/Account/UpdateAddress';
import ContactPerson from './Screens/Account/UpdateContactPerson';
import Account from './Screens/Account/AccountProfile';

import Capture from './Screens/Transactions/Capture';
import List from './Screens/Transactions/List';
import Details from './Screens/Transactions/Details';

import injectTapEventPlugin from 'react-tap-event-plugin';
injectTapEventPlugin();

var appHeader = 'Online Transaction Securities';
var appSubHeader = 'Feel Secured When Buying Online'; 

class App extends Component {
  state = {
    page: !sessionStorage.page || sessionStorage.page === '' ? "/Login" : sessionStorage.page
  }

  componentDidMount() { 
    var parent = this;
    window.addEventListener('RedirectPage', function(e) {
      parent.setState({ page: e.detail.page })
    });
  }

  render() {
    return (
      <MuiThemeProvider>
        <div className="App">
            <div className="App-header" style={{ backgroundImage: `url(${HeaderBackgound})`, backgroundSize: '100%' }}>
              <div className="App-bar">
                <div style={{display: "inline-block", width: "70%"}}>
                  <img src={logo} style={{width: 30}} />
                </div>
                <div className="User-Menu">
                  <User />
                </div>                
              </div>
              <h2 style={{color: "maroon"}}>{appHeader}</h2>
              <h5 style={{color: grey700}}>{appSubHeader}</h5>  
            </div>
            <Router>
              <Route render={({ location }) => (
                <div><RouteTransition 
                    pathname={location.pathname}
                      atEnter={{ translateX: 100 }}
                      atLeave={{ translateX: -100 }}
                      atActive={{ translateX: 0 }}
                      mapStyles={styles => ({ transform: `translateX(${styles.translateX}%)` })} >
                    <Switch key={location.key} location={location}>
                      <Redirect exact from="/" to={this.state.page} />
                      <Route exact path="/Login" component={Login}/>
                      <Route exact path="/Register" component={Register}/>
                      <Route exact path="/Address" component={Address}/>
                      <Route exact path="/ContactPerson" component={ContactPerson}/>
                      <Route exact path="/Account" component={Account}/>
                      <Route exact path="/Capture" component={Capture}/>
                      <Route exact path="/List" component={List}/>
                      <Route exact path="/Details" component={Details}/>
                    </Switch>
                  </RouteTransition>
                </div>
              )}/>
            </Router>
            <Notifications />
        </div>
      </MuiThemeProvider>
    );
  }
}

export default App;
