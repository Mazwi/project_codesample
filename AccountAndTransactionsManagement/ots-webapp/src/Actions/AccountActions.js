import AccountService from '../Services/AccountService';

var AccountActions = {
  createNew: async function(account, callback) {
    await AccountService.register(account, function(results) {        
        callback(results);
    })      
  }, 
  login: async function(userName, password, callback) {
    var credentials = {};
    credentials.userName = userName;
    credentials.password = password;
    await AccountService.login(credentials, function(results) {
        callback(results);
    })      
  },
  logout: async function(token, callback) {
    await AccountService.logout(token, function(results) {        
        callback(results);
    }) 
  },
  forgotPassword: function(userName) { 
    AccountService.forgotPassword(userName, function() {
      //Perform Task
    }) 
  }
}

export default AccountActions;