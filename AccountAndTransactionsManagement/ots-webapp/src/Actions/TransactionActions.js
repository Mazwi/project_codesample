import TransactionService from '../Services/TransactionService';

var TransactionActions = {
  create: async function(token, transaction, callback) {
    await TransactionService.createTransaction(token, transaction, function(results) {        
        callback(results);
    })      
  },
  get: async function(token, callback) {
    await TransactionService.getTransactions(token, function(results) {        
        callback(results);
    })      
  },
  updateStatus: async function(token, transactionId, status, callback){
    await TransactionService.updateTransactionStatus(token, transactionId, status, function(results) {        
        callback(results);
    }) 
  }
}

export default TransactionActions;