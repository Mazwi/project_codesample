import ProfileService from '../Services/ProfileService';

var AddressActions = {
  get: async function(token, callback) {
    await ProfileService.getAddress(token, function(results) {        
        callback(results);
    })      
  },
  update: async function(token, address, callback) {
    await ProfileService.updateAddress(token, address, function(results) {        
        callback(results);
    })      
  }
}

export default AddressActions;