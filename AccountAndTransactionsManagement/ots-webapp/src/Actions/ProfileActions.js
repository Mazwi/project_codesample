import ProfileService from '../Services/ProfileService';

var ProfileActions = {
  get: async function(token, callback) {
    await ProfileService.getProfile(token, function(results) {        
        callback(results);
    })      
  },
  update: async function(token, profile, callback) {
    await ProfileService.updateProfile(token, profile, function(results) {        
        callback(results);
    })      
  }
}

export default ProfileActions;