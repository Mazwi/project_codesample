import ProfileService from '../Services/ProfileService';

var ContactPersonActions = {
  get: async function(token, callback) {
    await ProfileService.getContactPerson(token, function(results) {        
        callback(results);
    })      
  },
  update: async function(token, contactPerson, callback) {
    await ProfileService.updateContactPerson(token, contactPerson, function(results) {        
        callback(results);
    })      
  }
}

export default ContactPersonActions;