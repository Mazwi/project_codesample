class ProductModel {	
		constructor (source, sourceId, email, mobileNumber, products, deliver) {			
			this.Id = 0,
			this.Source = source;
			this.SourceId = sourceId;
			this.PersonEmail = email;
			this.PersonMobileNumber = mobileNumber;
			this.Products = products;
			this.UseOtsDelivery = deliver;
		}
	}

export default ProductModel;
