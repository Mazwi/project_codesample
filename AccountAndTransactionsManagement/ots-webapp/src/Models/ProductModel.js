class ProductModel {	
		constructor (source, description, sourceId, picture, category, categoryId, price) {			
			this.Id = 0,
			this.Description = description;
			this.Source = source;
			this.SourceId = sourceId;
			this.Picture = picture;
			this.Category = category,
			this.CategoryId = categoryId,
			this.Quantity = 1;
			this.Price = price;
			this.Offers = [{ Amount: price }];
		}
	}

export default ProductModel;
