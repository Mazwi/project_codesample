var AccountService = {
  //http://otsapidev.azurewebsites.net
  address: "http://otsapidev.azurewebsites.net",
  apiController: "http://otsapidev.azurewebsites.net/api/Account/",
  login: async function(credentials, callback) { 

    var payload = 'username=' + credentials.userName + '&password=' + credentials.password + '&grant_type=password';

    await fetch(this.address + '/token', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded',
      },
      body: payload
    })
    .then((response) => response.json())
    .then((responseJson) => {      
      if(responseJson.error === undefined){
        callback(responseJson);
      }
      else{
        callback(responseJson);
      }
    })  
  }, 
  register: async function(account, callback) {
    console.log(this.apiController + "Register");
    await fetch(this.apiController + "Register", {
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(account)
    })
    .then((response) => response.json())
    .then((responseJson) => {  
      if(responseJson.error === undefined){
        callback(responseJson);
      }
      else{
        alert('Error: ' + responseJson.error + ' details: ' + responseJson.error_description)
      }
    })  
  },
  logout: async function(token, callback) {
    await fetch(this.apiController + "Logout", {
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Authorization': 'bearer ' + token
      }
    })
    .then((response) => response.json())
    .then((responseJson) => {  
      console.log(responseJson);
      if(responseJson.error === undefined){
        callback(responseJson);
      }
      else{
        alert('Error: ' + responseJson.error + ' details: ' + responseJson.error_description)
      }
    }) 
  },
  forgotPassword: function(userName) {   
  }
}

export default AccountService;