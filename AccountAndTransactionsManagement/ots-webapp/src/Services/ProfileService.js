var ProfileService = {
    url: "http://otsapidev.azurewebsites.net/api/",
    getProfile: async function(token, callback) {
      await fetch(this.url + "Account/GetUser", {
        method: 'GET',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
          'Authorization': 'bearer ' + token
        }
      })
      .then((response) => response.json())
      .then((responseJson) => {  
        if(responseJson === null || responseJson.error === undefined){
          callback(responseJson);
        }
        else{
          alert('Error: ' + responseJson.error + ' details: ' + responseJson.error_description)
        }
      })
    },
    updateProfile: async function(token, profile, callback) { 
      await fetch(this.url + "Account/Update", {
        method: 'POST',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
          'Authorization': 'bearer ' + token
        },
        body: JSON.stringify(profile)
      })
      .then((response) => response.json())
      .then((responseJson) => { 
        if(responseJson.error === undefined){
          callback(responseJson);
        }
        else{
          alert('Error: ' + responseJson.error + ' details: ' + responseJson.error_description)
        }
      })  
    },
    getAddress: async function(token, callback) {
      await fetch(this.url + "UserAddress", {
        method: 'GET',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
          'Authorization': 'bearer ' + token
        }
      })
      .then((response) => response.json())
      .then((responseJson) => {  
        if(responseJson.error === undefined){
          callback(responseJson);
        }
        else{
          alert('Error: ' + responseJson.error + ' details: ' + responseJson.error_description)
        }
      })
    },
    updateAddress: async function(token, address, callback) { 
      await fetch(this.url + "UserAddress", {
        method: 'POST',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
          'Authorization': 'bearer ' + token
        },
        body: JSON.stringify(address)
      })
      .then((response) => response.json())
      .then((responseJson) => {  
        if(responseJson.error === undefined){
          callback(responseJson);
        }
        else{
          alert('Error: ' + responseJson.error + ' details: ' + responseJson.error_description)
        }
      })  
    },
    getContactPerson: async function(token, callback) {
      await fetch(this.url + "ContactPerson", {
        method: 'GET',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
          'Authorization': 'bearer ' + token
        }
      })
      .then((response) => response.json())
      .then((responseJson) => {  
        if(responseJson === null || responseJson.error === undefined){
          callback(responseJson);
        }
        else{
          alert('Error: ' + responseJson.error + ' details: ' + responseJson.error_description)
        }
      })
    },
    updateContactPerson: async function(token, contactPerson, callback) { 
      await fetch(this.url + "ContactPerson", {
        method: 'POST',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
          'Authorization': 'bearer ' + token
        },
        body: JSON.stringify(contactPerson)
      })
      .then((response) => response.json())
      .then((responseJson) => {  
        if(responseJson.error === undefined){
          callback(responseJson);
        }
        else{
          alert('Error: ' + responseJson.error + ' details: ' + responseJson.error_description)
        }
      })  
    }
}

export default ProfileService;