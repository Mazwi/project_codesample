var TransactionService = {
    url: "http://localhost:59746/api/",
    createTransaction: async function(token, transaction, callback) { 
      await fetch(this.url + "Transaction", {
        method: 'POST',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
          'Authorization': 'bearer ' + token
        },
        body: JSON.stringify(transaction)
      })
      //.then((response) => response.json())
      .then((responseJson) => {  
        if(responseJson.status === 200){
          callback(responseJson);
        }
        else{
          alert('Error: ' + responseJson.error + ' details: ' + responseJson.error_description)
        }
      })  
    },
    getTransactions: async function(token, callback) { 
      await fetch(this.url + "Transaction", {
        method: 'GET',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
          'Authorization': 'bearer ' + token
        }
      })
      .then((response) => response.json())
      .then((responseJson) => {  
        if(responseJson.error === undefined){
          callback(responseJson);
        }
        else{
          alert('Error: ' + responseJson.error + ' details: ' + responseJson.error_description)
        }
      })  
    },
    updateTransactionStatus: async function(token, transactionId, transactionStatus, callback){
      await fetch(this.url + "Transaction/" + transactionId, {
        method: 'PUT',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
          'Authorization': 'bearer ' + token
        },
        body: JSON.stringify(transactionStatus)
      })
      //.then((response) => response.json())
      .then((responseJson) => {  
        if(responseJson.status === 200){
          callback(responseJson);
        }
        else{
          alert('Error: ' + responseJson.error + ' details: ' + responseJson.error_description)
        }
      })  
    }
}

export default TransactionService;