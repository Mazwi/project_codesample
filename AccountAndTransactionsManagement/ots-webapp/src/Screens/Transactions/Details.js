import React, { Component } from 'react';
import { grey700, grey50, lightBlue600, redA400 } from 'material-ui/styles/colors';
import Paper from 'material-ui/Paper';
import {Table, 
		  TableBody,
		  TableHeader,
		  TableHeaderColumn,
		  TableRow,
		  TableRowColumn} from 'material-ui/Table';
import Subheader from 'material-ui/Subheader';
import Divider from 'material-ui/Divider';
import RaisedButton from 'material-ui/RaisedButton';
import CircularProgress from 'material-ui/RaisedButton';
import LinearProgress from 'material-ui/LinearProgress';

import {notify} from 'react-notify-toast';

import _ from 'lodash';

import DisplayProductItem from '../../Components/DisplayProductItemComponent';
import TransactionUpdate from '../../Components/TransactionUpdateComponent';

import TransactionStatusModel from '../../Models/TransactionStatusModel';
import TransactionActions from '../../Actions/TransactionActions';

import moment from 'moment';
import numeral from 'numeral';
import RandFormat from '../../settings/RandFormat';

var info = {};

class Details extends Component {
	constructor(props) {
		super(props)
		info = JSON.parse(sessionStorage.selectedTransaction);
		
		this.state = {
			token: '',
			userName: '',
			processing: false,
			itemsTotal: 0,
			totalFees: 0
		}
	}

	componentWillMount(){				
		if(sessionStorage.accessToken === undefined || sessionStorage.accessToken === '') 
		{
			this.props.history.replace({ pathname: '/Login' })
		}
		else {
			var currentUserName = JSON.parse(sessionStorage.accessToken).userName;
			var currentUserToken = JSON.parse(sessionStorage.accessToken).access_token;
			
			var calculatedItemsTotal = _.sumBy(info.Products, function(product) {
				return product.Quantity * product.Offers[product.Offers.length - 1].Amount
			})

			var calculatedTotalFees = _.sumBy(info.Fees, function(fee) {
				return (fee.Category === 1) ? (fee.Rate * calculatedItemsTotal) : fee.Rate
			})
			
			this.setState({ 
				itemsTotal: calculatedItemsTotal, 
				totalFees: calculatedTotalFees,
				token: currentUserToken,
				userName: currentUserName 
			});
		}
	}

	_updateTransactionStatus = (statusUpdate) => {
		this.setState({processing: true})
		var that = this;

		TransactionActions.updateStatus(this.state.token, info.Id, statusUpdate, function(response){
			that.setState({ processing: false });								      	
	      	if(response.status === 200){
  				notify.show("Transaction Successfully Updated!", "custom", 5000, { background: lightBlue600, text: "#FFFFFF" });
  			}
  			else
  			{
  				notify.show("Error: Could not update Transaction", "custom", 15000, { background: redA400, text: "#FFFFFF" });  				
  			}
		});

	}

	_acceptTransactionHandler = () => {
    	var transactionStatus = new TransactionStatusModel(
			4, ''
		);
		this._updateTransactionStatus(transactionStatus);
	}

	_rejectTransactionHandler = (transactionStatus) => {		
		this._updateTransactionStatus(transactionStatus);
	}

	_payActionHandler = () => {
    	var transactionStatus = new TransactionStatusModel(
			7, ''
		);		
		this._updateTransactionStatus(transactionStatus);		
	}

	_confirmPaymentHandler = () => {
    	var transactionStatus = new TransactionStatusModel(
			8, ''
		);		
		this._updateTransactionStatus(transactionStatus);		
	}

	_handleBack = () => {
	 	this.props.history.push({
		     pathname: '/List'
		 });
	}

	render() {
		//this.props.location;
		
		return(
			<Paper style={styles.container} zDepth={2}>
				{this.state.processing ? <LinearProgress mode="indeterminate" /> : ''}
				<div><Subheader>Transaction Details</Subheader>
					<div style={{display: "inline-block", minWidth: 240, maxWidth: 320, marginRight: 80}}>
						<span style={styles.label}><strong>Reference:</strong> {info.Reference}</span><br /><br />
						<span style={styles.label}><strong>Source Id/Ref:</strong> {info.SourceId}</span><br /><br />						
						<span style={styles.label}><strong>Create Date:</strong> {moment(info.CapturedDate).format('MMM DD, YYYY HH:mm:ss')}</span><br /><br />
						<span style={styles.label}><strong>Status:</strong> {info.StatusText}</span><br /><br />
						<span style={styles.label}><strong>Total Fees:</strong> {numeral(this.state.totalFees).format('$0,0.00')}</span>					
					</div>
					<div style={{display: "inline-block"}}>
						<span style={styles.label}><strong>Source:</strong> {info.Source}</span><br /><br />
						<span style={styles.label}><strong>{info.Buyer === this.state.userName ? "Seller: " + info.Seller : "Buyer: " + info.Buyer}</strong>
								&nbsp; {info.PersonMobileNumber} {info.PersonEmail} </span><br /><br />
						<span style={styles.label}><strong>Last Updated:</strong> {moment(info.LastUpdateDate).format('MMM DD, YYYY HH:mm:ss')}</span><br /><br />
						<span style={styles.label}><strong>Deliver:</strong> {info.UseOtsDelivery ? "Yes" : "No"}</span><br /><br />
						<span style={styles.label}><strong>Total:</strong> {numeral(this.state.itemsTotal + this.state.totalFees).format('$0,0.00')}</span>
					</div>
					<br /><br /><Divider /><br />
				</div>
				<TransactionUpdate 	seller={info.Seller} status={info.Status}
									backToListActionHangler={this._handleBack}
									acceptActionHandler={this._acceptTransactionHandler}
									rejectActionHandler={this._updateTransactionStatus}
									payActionHandler={this._payActionHandler}
									confirmPaymentActionHandler={this._confirmPaymentHandler} />
				<div>
					<br /><Divider /><br />
					<Subheader>Purchased Products</Subheader>
					{info.Products.map((product, i) => <DisplayProductItem itemIndex={1} item={product} />)}
					<br /><br /><Divider /><br />
				</div>
				<div style={styles.feesPanel}>
					<Subheader>Fees</Subheader>
					{info.Fees.map((fee, i) => (<div><strong style={styles.label}>{fee.Description} </strong>
														<span>{numeral((fee.Category === 1) ? (fee.Rate * this.state.itemsTotal) : fee.Rate).format('$0,0.00')}</span><br /><br /></div>))}
				</div>
				<div style={styles.commentsPanel}>
				<Subheader>Logs</Subheader>
				{info.Comments.map((comment, i) => (<div><span style={styles.label}>{moment(comment.CapturedDate).format('MMM DD, HH:mm:ss')}: </span>
														<span>{comment.CapturedBy} > </span>
														<span>{comment.StatusText} </span>
														<span>{comment.Comment}</span></div>))}
				</div>

			</Paper>			
		)
	}
}

const styles = {
	container: {
		backgroundColor: grey50,
      	fontFamily: "Roboto, sans-serif",
      	fontSize: 14,
		display: "inline-block",
		maxWidth: 920,
		width: "100%",
		textAlign: "left",
		padding: 20,
		marginTop: 10
	},
	label: {
		padding: 10,
		paddingRight: 30,
		paddingTop: 20,
		color: grey700,
	},
	feesPanel: {
		display: "inline-block", 
		width: "100%",
		maxWidth: 260
	},
	commentsPanel: {
		display: "inline-block", 
		width: "100%",
		maxWidth: 580
	}
}

export default Details