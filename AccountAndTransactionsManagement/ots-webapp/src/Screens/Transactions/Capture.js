import React, { Component } from 'react';
import TextField from 'material-ui/TextField';
import RaisedButton from 'material-ui/RaisedButton';
import FlatButton from 'material-ui/FlatButton';
import Subheader from 'material-ui/Subheader';
import FloatingActionButton from 'material-ui/FloatingActionButton';
import ContentAdd from 'material-ui/svg-icons/content/add';
import Toggle from 'material-ui/Toggle';
import { lightBlue600, grey50, grey200, grey700, limeA100, redA400 } from 'material-ui/styles/colors';
import CircularProgress from 'material-ui/CircularProgress';
import Paper from 'material-ui/Paper';

import _ from 'lodash';
import {notify} from 'react-notify-toast';

import defaultPicture from '../../images/shopping-bag.png';

import AddProduct from '../../Components/AddProductComponent';
import ProductItem from '../../Components/ProductItemComponent';

import TransactionActions from '../../Actions/TransactionActions';

import ProductModel from '../../Models/ProductModel';
import TransactionModel from '../../Models/TransactionModel';

class Capture extends Component {
	constructor(props) {
    	super(props);

    	this.state = {
			accessToken: '',
    		processing: false,
    		addProduct: <div style={{textAlign: "right", paddingRight: 60}}>
    						<FloatingActionButton mini={true} style={styles.addProductButton} onTouchTap={this.addNewProduct}>
					      		<ContentAdd />
					    	</FloatingActionButton>
				    	</div>,
	      	products: [],
	      	source: '',
	      	sourceId: '',
	      	deliver: false,
	      	sellerEmail: '',
	      	sellerMobile: ''
      	};
    }

	componentWillMount(){
		
		if(sessionStorage.accessToken === undefined || sessionStorage.accessToken === '') 
		{
			this.props.history.replace({ pathname: '/Login' })
		}
		else {      	
			var currentUserToken = JSON.parse(sessionStorage.accessToken).access_token;	
			var receivedProductMock = new ProductModel(
				"http://onlinefleamarket.co.za/index.php?page=item&id=22",
				"Mountain Bike",
				"1234",
				defaultPicture,
				"",
				1,
				2000.41
			)

			this.setState({ 
				accessToken: currentUserToken,
				products: [receivedProductMock],
				source: receivedProductMock.Source,
				sourceId: receivedProductMock.SourceId
			});		
		}
	}

	addProductHandler = (newProduct) => {
		this.state.products.push(newProduct);
		
	    this.setState({ products: this.state.products, 
	    				addProduct: 
							<div style={{textAlign: "right", paddingRight: 60}}>
								<FloatingActionButton mini={true} style={styles.addProductButton} onTouchTap={this.addNewProduct}>
						      		<ContentAdd />
						    	</FloatingActionButton>
					    	</div> })
	}

	cancelProductHandler = () => {		
	    this.setState({ addProduct: 
							<div style={{textAlign: "right", paddingRight: 60}}>
								<FloatingActionButton mini={true} style={styles.addProductButton} onTouchTap={this.addNewProduct}>
						      		<ContentAdd />
						    	</FloatingActionButton>
					    	</div> })
	}

	addNewProduct = async () => {
		this.setState({addProduct: <AddProduct 	sourceUrl={this.state.source}
												addProductCallback={this.addProductHandler}
												cancelProductCallback={this.cancelProductHandler} />})
	}

	removeProduct(item) {
		this.state.products.splice(item, 1);
		this.setState({ products: this.state.products });
	}

	_handleQtyChange = (itemIndex, qty) => {
		_.map(this.state.products, function(product, i) {
				if(itemIndex === i){
					product.quantity = qty
					return product
				}
			}
		)
		console.log(this.state.products)		
	}

	_handleOfferChange = (itemIndex, offer) => {
		_.map(this.state.products, function(product, i) {
				if(itemIndex === i){
					var productOffer = { Amount: offer, Comment: "First Offer"}
					product.Offers[0] = productOffer;
					return product
				}
			}
		)
		console.log(itemIndex + '##' + offer)		
	}

	_handleDeliverChange = (event, isInputChecked) => {
		this.setState({ deliver: isInputChecked });
	}

	_handleSellerEmailChange = (e) => {
        this.setState({ sellerEmail: e.target.value });
    }

 	_handleSellerMobelNumberChange = (e) => {
        this.setState({ sellerMobile: e.target.value });
    }

    placeOrder = async () => {
    	var transaction = new TransactionModel(
    			this.state.source,
    			this.state.sourceId,
    			this.state.sellerEmail,
    			this.state.sellerMobile,
    			this.state.products,
    			this.state.deliver
    		)
    	this.setState({ processing: true });
		var that = this;
		TransactionActions.create(this.state.accessToken, transaction, function(response) {
			that.setState({ processing: false });			
		 	that.props.history.push({
			     pathname: '/List'
			 	});			
	      	
	      	if(response.status === 200){
  				notify.show("Transaction Successfully Captured!", "custom", 5000, { background: lightBlue600, text: "#FFFFFF" });
  			}
  			else
  			{
  				notify.show("Error: Could not create Transaction", "custom", 15000, { background: redA400, text: "#FFFFFF" });  				
  			}
		});
    }

	render() {
		var productsList = _.map(this.state.products, (product, i) => 
	    			<div><div key={i} style={styles.productItem}>
	    				<div style={{display: "inline-block"}}>
			        		<FlatButton label="X" secondary={true} style={{margin: 2}} onTouchTap={this.removeProduct.bind(this, i)} />
			        		<br /><br />
	    				</div>
	    				<ProductItem item={product} 
	    					itemIndex={i}
	    					qtyChangeCallback={this._handleQtyChange}
	    					offerChangeCallback={this._handleOfferChange}  />
    				</div><br /></div>)
    	return (  		
	    		<Paper zDepth={2} style={styles.container}>
					<h1 style={{color: grey700}}>Create Transaction</h1>
	    			<div>
	    				{this.state.addProduct}
	    			</div>	
    				{productsList}					    
					<div style={{textAlign: "center", display: "inline-block", color: "white"}}>
						<Toggle
							label="Use OTS Delivery"
							labelPosition="right"
							labelStyle={{color: lightBlue600}}
							onToggle={this._handleDeliverChange} />
					</div>
					<Subheader>Seller Contact Details</Subheader>
					<TextField type="email" 
						hintText="Email" 
						floatingLabelText="Email" 
						style={{marginRight: 20}} 
						value={this.state.sellerEmail} 
						onChange={this._handleSellerEmailChange} />
					<TextField
						hintText="Mobile Number" 
						floatingLabelText="Mobile Number" 
						style={{marginRight: 20}} 
						value={this.state.sellerMobile} 
						onChange={this._handleSellerMobelNumberChange} />
					<br />
					<RaisedButton label="Submit Order" primary={true} style={{margin: 20}} disabled={this.state.processing} onTouchTap={this.placeOrder} /><br /> 
					{this.state.processing ? <CircularProgress size={45} thickness={5} /> : ''}
	    		</Paper>
    	)
	}
}

const styles = {
	container: {
		textAlign: "center",
		backgroundColor: grey50,
      	fontFamily: "Roboto, sans-serif",
      	fontSize: 14,
		display: "inline-block",
		maxWidth: 920,
		width: "100%",
		position: "relative",
		marginTop: 10
	},
	productItem: {
		display: "inline-block",
		width: "80%",
		maxWidth: 680,
		textAlign: "center",
		backgroundColor: grey200,
		margin: 10,
		padding: 10,
		borderRadius: 10
	},
	addProductButton: {
		margin: 15, 
		position: "absolute", 
		top: -34
	}
}

export default Capture;