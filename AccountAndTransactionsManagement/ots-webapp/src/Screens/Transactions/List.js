import React, { Component } from 'react';
import {  Redirect, withRouter } from 'react-router-dom';
import {Tabs, Tab} from 'material-ui/Tabs';
import Paper from 'material-ui/Paper';
import Subheader from 'material-ui/Subheader';
import {Table, 
		  TableBody,
		  TableHeader,
		  TableHeaderColumn,
		  TableRow,
		  TableRowColumn} from 'material-ui/Table';
import LinearProgress from 'material-ui/LinearProgress';
import { grey50, grey700 } from 'material-ui/styles/colors'

import _ from 'lodash';

import Transactions from '../../Components/TransactionsComponent';

import TransactionActions from '../../Actions/TransactionActions';

import TransactionModel from '../../Models/TransactionModel';

class List extends Component {	
	state = {
		processing: true,
		userName: '',
		transactions: [],
		selectedTransaction: {}
	}

	componentDidMount() {
		var that = this;
		
		if(sessionStorage.accessToken === undefined || sessionStorage.accessToken === '') 
		{
			this.props.history.replace({ pathname: '/Login' })
		}
		else {
			var currentUserName = JSON.parse(sessionStorage.accessToken).userName;
			var currentUserToken = JSON.parse(sessionStorage.accessToken).access_token;
				
			TransactionActions.get(currentUserToken, function(results) {
				that.setState({ 
						processing: false, 
						transactions: results,
						userName: currentUserName
					});
			});
		}
	}

	_goToDetails = (transaction) => {
		sessionStorage.selectedTransaction = JSON.stringify(transaction);
		this.props.history.replace({
		     pathname: '/Details',
		     state: {test: 'transaction'} 
		})
	}

	render(){
		var that = this;
		var buying = _.filter(this.state.transactions, function(transaction) {
			return transaction.Buyer === that.state.userName;
		});

		var selling = _.filter(this.state.transactions, function(transaction) {
			return transaction.Seller === that.state.userName;
		});

		var closed = _.filter(this.state.transactions, function(transaction) {
			return transaction.Status === 25;
		});
 		
		return (
			<Paper zDepth={1} style={containerStyles}>
				<h1 style={{color: grey700}}>Your Transactions</h1>		
				<Tabs style={{maxWidth: 920, display: "inline-block", marginTop: 20}}>
					<Tab label="Open">
						<br /> {this.state.processing ? <LinearProgress mode="indeterminate" /> : ''}		
			    		{(buying.length > 0) ? 
						<Paper zDepth={0}>
							<Subheader>Buying</Subheader>
							<Transactions currentUser={this.state.userName} transactions={buying} onReferenceCellClick={this._goToDetails} />
						</Paper> : ''}
						<br /><br />
			    		{(selling.length > 0) ? 
							<Paper zDepth={0}>
								<Subheader>Selling</Subheader>
								<Transactions currentUser={this.state.userName} transactions={selling} onReferenceCellClick={this._goToDetails} />
							</Paper> : ''}
						<br /><br />
					</Tab>
					<Tab label="Closed">
						{this.state.processing ? <LinearProgress mode="indeterminate" /> : ''}<br />
			    		<Paper zDepth={1}>
							<Table>
								<TableHeader>
									<TableRow>							
										<TableHeaderColumn></TableHeaderColumn>
										<TableHeaderColumn>Date</TableHeaderColumn>
										<TableHeaderColumn>Reference</TableHeaderColumn>
										<TableHeaderColumn>Buyer/Seller</TableHeaderColumn>	
										<TableHeaderColumn>Amount R</TableHeaderColumn>
									</TableRow>
								</TableHeader>
								<TableBody>
									<TableRow>
										<TableRowColumn>Bought</TableRowColumn>
										<TableRowColumn>2017/06/06</TableRowColumn>
										<TableRowColumn>109809124</TableRowColumn>
										<TableRowColumn>Promise Thwala</TableRowColumn>
										<TableRowColumn>2000.00</TableRowColumn>
									</TableRow>
									<TableRow>
										<TableRowColumn>Sold</TableRowColumn>
										<TableRowColumn>2017/06/06</TableRowColumn>
										<TableRowColumn>109809124</TableRowColumn>
										<TableRowColumn>Mazwi Thwala</TableRowColumn>
										<TableRowColumn>2000.00</TableRowColumn>
									</TableRow>
								</TableBody>
							</Table>
			    		</Paper>
						<br /><br />
					</Tab>
				</Tabs>
			</Paper>
		)
	}
}

const containerStyles = {
	textAlign: "center",
	backgroundColor: grey50,
  	fontFamily: "Roboto, sans-serif",
  	fontSize: 14,
	display: "inline-block",
	maxWidth: 920,
	width: "100%",
	padding: 30,
	marginTop: 10
}

export default List