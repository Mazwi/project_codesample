import React, { Component } from 'react';
import {  Redirect } from 'react-router-dom'
import Paper from 'material-ui/Paper';
import TextField from 'material-ui/TextField';
import RaisedButton from 'material-ui/RaisedButton';
import FlatButton from 'material-ui/FlatButton';
import AccountActions from '../../Actions/AccountActions';

import { lightBlue600, redA400 } from 'material-ui/styles/colors'

import CircularProgress from 'material-ui/CircularProgress';
import {notify} from 'react-notify-toast';

const panel = {
  margin: 20,
  textAlign: 'center',
  display: 'inline-block',
  paddingLeft: 30,
  paddingRight: 10,
  paddingBottom: 20
};

class Register extends Component {
	constructor(props) {
    	super(props);
    	this.state = {
    		//createCnt: 0,
    		redirectToLogin: false,
    		processing: false,
	    	firstName: '',
	    	surname: '',
	      	email: '', 
	      	primaryContacts: '', 
	      	password: '',
	      	confirmPassword: ''
      	};
    }

  	login = () => {
    	this.setState({ redirectToLogin: true })
  	}

	createAccount = async () => {
		var account = {};
        account.FirstName = this.state.firstName; //"Test " + ++this.state.createCnt; 
        account.Surname = this.state.surname; //"Testing";
		account.Email = this.state.email; //"test" + this.state.createCnt + "@testing.com";
        account.PrimaryContacts = this.state.primaryContacts; //"0112224455";
		account.Password = this.state.password; //"MZ888.net";
		account.ConfirmPassword = this.state.confirmPassword; //"MZ888.net";
		
		var requestFailed = true;
		this.setState({ processing: true });

		await AccountActions.createNew(account, function(response){
	      	if(response.Message === undefined || response.Message === ""){
  				requestFailed = false;
  				notify.show("Account Successfully Registered!", "custom", 5000, { background: lightBlue600, text: "#FFFFFF" });
  			}
  			else
  			{
  				notify.show("Error: Could not register new account", "custom", 15000, { background: redA400, text: "#FFFFFF" });  				
  			}
		});

		this.setState({ processing: false });
		if(!requestFailed) this.login();
	}

	 _handleFirstNameChange = (e) => {
        this.setState({ firstName: e.target.value });
    }
	 _handleSurnameChange = (e) => {
        this.setState({ surname: e.target.value });
    }
	 _handleEmailChange = (e) => {
        this.setState({ email: e.target.value });
    }
	 _handleContactsChange = (e) => {
        this.setState({ primaryContacts: e.target.value });
    }
	 _handlePasswordChange = (e) => {
        this.setState({ password: e.target.value });
    }
	 _handleConfirmChange = (e) => {
        this.setState({ confirmPassword: e.target.value });
    }

	render() {

		let formAction = null;
		if(this.state.processing){
			formAction = <CircularProgress size={60} thickness={5} />;
		} else {
			formAction = <div><RaisedButton label="Create" style={{margin: 20}} onTouchTap={this.createAccount} /><br />
								<FlatButton label="Already Have an Account (Login)" primary={true} style={{margin: 10}} onTouchTap={this.login} /></div>;			
		}	

    	return (
				this.state.redirectToLogin ? <Redirect to={'/Login'}/> :   		
	    		<Paper style={panel} zDepth={3}>
	    			<h4>Create New Account</h4>
	    			<TextField hintText="First Name" floatingLabelText="First Name" style={{marginRight: 20}} value={this.state.firstName} onChange={this._handleFirstNameChange} />
	    			<TextField hintText="Surname" floatingLabelText="Surname" style={{marginRight: 20}} value={this.state.surname} onChange={this._handleSurnameChange} /><br />
	    			<TextField hintText="Email (username)" floatingLabelText="Email (username)" style={{marginRight: 20}} value={this.state.email} onChange={this._handleEmailChange}  />
	    			<TextField hintText="Primary Contacts" floatingLabelText="Primary Contacts" style={{marginRight: 20}} value={this.state.primaryContacts} onChange={this._handleContactsChange} /><br />
	    			<TextField hintText="Password" floatingLabelText="Password" type="password" style={{marginRight: 20}} value={this.state.password} onChange={this._handlePasswordChange} />
	    			<TextField hintText="Confirm Password" floatingLabelText="Confirm Password" type="password" style={{marginRight: 20}} value={this.state.confirmPassword} onChange={this._handleConfirmChange} /><br /><br />
	    			{formAction}
	    		</Paper>
    	)
	}
}

export default Register;