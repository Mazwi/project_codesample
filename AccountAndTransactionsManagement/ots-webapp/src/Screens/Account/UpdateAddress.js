import React, { Component } from 'react';
import {  Redirect } from 'react-router-dom';
import Paper from 'material-ui/Paper';
import TextField from 'material-ui/TextField';
import RaisedButton from 'material-ui/RaisedButton';
import AddressActions from '../../Actions/AddressActions';

import { lightBlue600, redA400 } from 'material-ui/styles/colors'

import CircularProgress from 'material-ui/CircularProgress';
import {notify} from 'react-notify-toast';

const panel = {
  margin: 20,
  textAlign: 'center',
  display: 'inline-block',
  paddingLeft: 30,
  paddingRight: 10,
  paddingBottom: 20
};

class UpdateAddress extends Component {
	constructor(props) {
    	super(props);
    	this.state = {
    		accessToken: '',
    		redirectToContactPerson: false,
    		processing: false,
    		userAddressId: 0,
    		addressId: 0,
	    	street: '',
	    	suburb: '',
	      	city: '', 
	      	province: '', 
	      	code: '',
	      	country: ''
      	};
    }

  	continue = () => {
    	this.setState({ redirectToContactPerson: true })
  	}

  	async componentDidMount() {  
			
		if(sessionStorage.accessToken === undefined || sessionStorage.accessToken === '') 
		{
			this.props.history.push({ pathname: '/Login' })
		}
		else {      	
			var currentUserToken = JSON.parse(sessionStorage.accessToken).access_token;
			this.setState({ processing: true, accessToken: currentUserToken });

			var that = this;
			await AddressActions.get(currentUserToken, function(userAddresses){
				if(userAddresses.length !== undefined && userAddresses.length !== 0){
					var address = userAddresses[0].Address;
					that.setState({
						processing: false,
						userAddressId: address.UserAddressId,
						addressId: address.AddressId, 
						street: address.StreetAddress,
						suburb: address.Suburb,
						city: address.City, 
						province: address.Province, 
						code: address.AreaCode,
						country: address.Country
					});
				} else {
					that.setState({ processing: false });
				}
			});
		}
  	}

	updateAddress = async () => {
		var address = {};
		address.AddressId = this.state.addressId;
        address.StreetAddress = this.state.street;
        address.Suburb = this.state.suburb;
		address.City = this.state.city;
        address.Province = this.state.province;
		address.AreaCode = this.state.code;
		address.Country = this.state.country;
		
		var userAddress = { UserAddressId: this.state.userAddressId, Address: address };
		
		var requestFailed = true;
		this.setState({ processing: true });

		await AddressActions.update(this.state.accessToken, userAddress, function(response){
	      	if(response.Message === undefined || response.Message === ""){
  				requestFailed = false;
  				notify.show("Address Successfully Updated!", "custom", 5000, { background: lightBlue600, text: "#FFFFFF" });
  			}
  			else
  			{
  				notify.show("Error: Could not update address", "custom", 15000, { background: redA400, text: "#FFFFFF" });  				
  			}
		});

		this.setState({ processing: false });
		if(!requestFailed) this.continue();
	}

	 _handleStreetChange = (e) => {
        this.setState({ street: e.target.value });
    }
	 _handleSuburbChange = (e) => {
        this.setState({ suburb: e.target.value });
    }
	 _handleCityChange = (e) => {
        this.setState({ city: e.target.value });
    }
	 _handleProvinceChange = (e) => {
        this.setState({ province: e.target.value });
    }
	 _handleCodeChange = (e) => {
        this.setState({ code: e.target.value });
    }
	 _handleCountryChange = (e) => {
        this.setState({ country: e.target.value });
    }

	render() {

		let formAction = null;
		if(this.state.processing){
			formAction = <CircularProgress size={60} thickness={5} />;
		} else {
			formAction = <div><RaisedButton label="Update" style={{margin: 20}} onTouchTap={this.updateAddress} /></div>;			
		}	

    	return (
				this.state.redirectToContactPerson ? <Redirect to={'/ContactPerson'}/> :   		
	    		<Paper style={panel} zDepth={3}>
	    			<h4>Address</h4>
	    			<TextField hintText="Street Address" floatingLabelText="Street Address" style={{marginRight: 20}} value={this.state.street} onChange={this._handleStreetChange} />
	    			<TextField hintText="Suburb" floatingLabelText="Suburb" style={{marginRight: 20}} value={this.state.suburb} onChange={this._handleSuburbChange} /><br />
	    			<TextField hintText="City" floatingLabelText="City" style={{marginRight: 20}} value={this.state.city} onChange={this._handleCityChange}  />
	    			<TextField hintText="Province / State" floatingLabelText="Province / State" style={{marginRight: 20}} value={this.state.province} onChange={this._handleProvinceChange} /><br />
	    			<TextField hintText="Area Code" floatingLabelText="Area Code" style={{marginRight: 20}} value={this.state.code} onChange={this._handleCodeChange} />
	    			<TextField hintText="Country" floatingLabelText="Country" style={{marginRight: 20}} value={this.state.country} onChange={this._handleCountryChange} /><br /><br />
	    			{formAction}
	    		</Paper>
    	)
	}
}

export default UpdateAddress;