import React, { Component } from 'react';
import { Tabs, Tab } from 'material-ui/Tabs'

import Profile from './UpdateProfile';
import Address from './UpdateAddress';
import ContactPerson from './UpdateContactPerson';

class AccountProfile extends Component {


  	componentDidMount() { 
      if(sessionStorage.accessToken === undefined || sessionStorage.accessToken === '') 
      {
        this.props.history.push({ pathname: '/Login' })
      }
    }
	render() {
		return(
      <div>
        <h2>My Account</h2>
        <Tabs style={{maxWidth: 920, display: "inline-block", marginTop: 20}} tabItemContainerStyle={{textAlign: "center"}}>
					<Tab label="Personal Details">
            <Profile /><br />
          </Tab>
					<Tab label="Addresses">
            <Address />
          </Tab>
					<Tab label="Contact Person">
            <div style={{textAlign: "center"}}>
                <ContactPerson />
            </div>
          </Tab>
        </Tabs>
      </div>
  	)
	}
}

export default AccountProfile;