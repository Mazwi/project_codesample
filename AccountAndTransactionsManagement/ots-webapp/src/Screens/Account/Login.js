import React, { Component } from 'react';
import {  Redirect, withRouter } from 'react-router-dom'
import Paper from 'material-ui/Paper';
import TextField from 'material-ui/TextField';
import RaisedButton from 'material-ui/RaisedButton';
import FlatButton from 'material-ui/FlatButton';
import AccountActions from '../../Actions/AccountActions';

import { lightBlue600, redA400 } from 'material-ui/styles/colors';

import CircularProgress from 'material-ui/CircularProgress';

import {notify} from 'react-notify-toast';

const panel = {
  margin: 20,
  textAlign: 'center',
  display: 'inline-block',
  padding: 35
};

class Auth extends Component {
	constructor(props) {
		super(props);
    	this.state = {
    		redirectTo: '',
    		processing: false,
	    	username: '',
	      	password: ''
      	};

        sessionStorage.page = "/Login";
		const appHeader = 'Welcome To Online Transaction Securities';
    }

  	register = () => {
    	this.setState({ redirectTo: '/Register' })
  	}

  	updateAddress = () => {
    	this.setState({ redirectTo: '/Address' })
  	}

  	login = async () => {
		var username = this.state.username;
		var password = this.state.password;

		this.setState({ processing: true });
		var that = this;
		await AccountActions.login(username, password, function(response){
	      	if(response.access_token !== undefined && response.access_token !== ""){
  				sessionStorage.accessToken = JSON.stringify(response);
          		sessionStorage.authenticated = true;
  				notify.show("Logged-in Successfully!", "custom", 5000, { background: lightBlue600, text: "#FFFFFF" });
				that.props.history.push({ pathname: '/Address' })				
				//this.updateAddress(); 
        		var loggedInEvent = new CustomEvent('loggedIn', { data: that.state });
        		window.dispatchEvent(loggedInEvent);
			}
			else
			{
				notify.show("Error: " + response.error_description, "custom", 15000, { background: redA400, text: "#FFFFFF" });  				
			}
  		});			 
  	}

	 _handleUsernameChange = (e) => {
        this.setState({ username: e.target.value });
    }
	 _handlePasswordChange = (e) => {
        this.setState({ password: e.target.value });
    }

	render() {

		let formAction = null;
		if(this.state.processing){
			formAction = <CircularProgress size={60} thickness={5} />;
		} else {
			formAction = <div><RaisedButton label="Login" style={{margin: 10}} onClick={this.login} /><br />
								<FlatButton label="Create New Account" primary={true} style={{margin: 10}} onClick={this.register} /></div>;			
		}

		return(
			this.state.redirectTo !== '' ? <Redirect to={this.state.redirectTo}/> :
			<div style={{height: "70vh"}}>
	    		<Paper style={panel} zDepth={3}>
	    			<h4>Login</h4>
	    			<TextField hintText="Username" floatingLabelText="Username" value={this.state.username} onChange={this._handleUsernameChange}  /><br />
	    			<TextField hintText="Password" floatingLabelText="Password" type="password" value={this.state.password} onChange={this._handlePasswordChange}  /><br /><br />	    				    			
	    			{formAction}
	    		</Paper>
    		</div>
    	)
	}
}

export default Auth;