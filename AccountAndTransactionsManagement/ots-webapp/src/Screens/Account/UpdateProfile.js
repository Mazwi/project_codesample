import React, { Component } from 'react';
import {  Redirect } from 'react-router-dom'
import Paper from 'material-ui/Paper';
import TextField from 'material-ui/TextField';
import RaisedButton from 'material-ui/RaisedButton';
import ProfileActions from '../../Actions/ProfileActions';

import { lightBlue600, redA400 } from 'material-ui/styles/colors'

import CircularProgress from 'material-ui/CircularProgress';
import {notify} from 'react-notify-toast';

const panel = {
  margin: 20,
  textAlign: 'center',
  display: 'inline-block',
  paddingLeft: 30,
  paddingRight: 10,
  paddingBottom: 20
};

class UpdateProfile extends Component {
	constructor(props) {
    	super(props);
    	this.state = {
    		accessToken: '',
    		redirectToLogin: false,
    		processing: false,
	    	firstName: '',
	    	surname: '',
	      	email: '', 
	      	primaryContacts: '', 
	      	username: ''
	      };
    }

  	async componentDidMount() {  
		
		if(sessionStorage.accessToken === undefined || sessionStorage.accessToken === '') 
		{
			this.props.history.push({ pathname: '/Login' })
		}
		else {      	
			var currentUserToken = JSON.parse(sessionStorage.accessToken).access_token;
			this.setState({ processing: true, accessToken: currentUserToken });
			
			var that = this;
			await ProfileActions.get(currentUserToken, function(profile){
				if(profile !== null && profile !== undefined && profile.Name !== ""){
					that.setState({
						processing: false,
						firstName: profile.FirstName,
						surname: profile.Surname, 
						email: profile.Email,
						primaryContacts: profile.PrimaryContacts,
						username: profile.UserName,
						passwordHash: profile.passwordHash
					});
				} else {
					that.setState({ processing: false });
				}
			});
		}
  	}

	updateProfile = async () => {
		var profile = {};
		profile.Username = this.state.username;
        profile.FirstName = this.state.firstName;
        profile.Surname = this.state.surname;
		profile.Email = this.state.email;
        profile.PrimaryContacts = this.state.primaryContacts;
		
		var requestFailed = true;
		this.setState({ processing: true });

		await ProfileActions.update(this.state.accessToken, profile, function(response){
	      	if(response.Message === undefined || response.Message === ""){
  				requestFailed = false;
  				notify.show("Account Successfully Updated!", "custom", 5000, { background: lightBlue600, text: "#FFFFFF" });
  			}
  			else
  			{
  				notify.show("Error: Could not update account", "custom", 15000, { background: redA400, text: "#FFFFFF" });  				
  			}
		});

		this.setState({ processing: false });
	}

	 _handleFirstNameChange = (e) => {
        this.setState({ firstName: e.target.value });
    }
	 _handleSurnameChange = (e) => {
        this.setState({ surname: e.target.value });
    }
	 _handleEmailChange = (e) => {
        this.setState({ email: e.target.value });
    }
	 _handleContactsChange = (e) => {
        this.setState({ primaryContacts: e.target.value });
    }
	 _handleUsernameChange = (e) => {
        this.setState({ username: e.target.value });
    }

	render() {

		let formAction = null;
		if(this.state.processing){
			formAction = <CircularProgress size={60} thickness={5} />;
		} else {
			formAction = <div><RaisedButton label="Update" style={{margin: 20}} onTouchTap={this.updateProfile} /><br /></div>;			
		}	

    	return (
				this.state.redirectToLogin ? <Redirect to={'/Login'}/> :   		
	    		<Paper style={panel} zDepth={3}>
	    			<h4>Update Profile Info</h4>
	    			<TextField hintText="First Name" floatingLabelText="First Name" style={{marginRight: 20}} value={this.state.firstName} onChange={this._handleFirstNameChange} />
	    			<TextField hintText="Surname" floatingLabelText="Surname" style={{marginRight: 20}} value={this.state.surname} onChange={this._handleSurnameChange} /><br />
	    			<TextField hintText="Email" floatingLabelText="Email" style={{marginRight: 20}} value={this.state.email} onChange={this._handleEmailChange}  />
	    			<TextField hintText="Primary Contacts" floatingLabelText="Primary Contacts" style={{marginRight: 20}} value={this.state.primaryContacts} onChange={this._handleContactsChange} /><br />
	    			<TextField hintText="Username" floatingLabelText="Username" style={{marginRight: 20}} value={this.state.username} onChange={this._handleUsernameChange} />
	    			<TextField hintText="Password" floatingLabelText="Password" type="password" disabled style={{marginRight: 20}} value="**********" /><br /><br />
	    			{formAction}
	    		</Paper>
    	)
	}
}

export default UpdateProfile;