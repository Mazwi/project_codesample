import React, { Component } from 'react';
import {  Redirect } from 'react-router-dom'
import Paper from 'material-ui/Paper';
import TextField from 'material-ui/TextField';
import RaisedButton from 'material-ui/RaisedButton';
import ContactPersonActions from '../../Actions/ContactPersonActions';

import { lightBlue600, redA400 } from 'material-ui/styles/colors'

import CircularProgress from 'material-ui/CircularProgress';
import {notify} from 'react-notify-toast';

const panel = {
  margin: 20,
  textAlign: 'center',
  display: 'inline-block',
  paddingLeft: 30,
  paddingRight: 10,
  paddingBottom: 20
};

class UpdateContactPerson extends Component {
	constructor(props) {
    	super(props);
    	this.state = {
    		accessToken: '',
    		redirectToLogin: false,
    		processing: false,
    		contactPersonId: 0,
	    	name: '',
	      	tel: '',
	    	cell: ''
      	};
    }

  	continue = () => {
    	this.setState({ redirectToTransaction: true })
  	}

  	async componentDidMount() {  
			
		if(sessionStorage.accessToken === undefined || sessionStorage.accessToken === '') 
		{
			this.props.history.push({ pathname: '/Login' })
		} 
		else {     	
			var currentUserToken = JSON.parse(sessionStorage.accessToken).access_token;
			this.setState({ processing: true, accessToken: currentUserToken });

			var that = this;
			await ContactPersonActions.get(currentUserToken, function(contactPerson){
				if(contactPerson !== null && contactPerson !== undefined && contactPerson.Name !== ""){
					that.setState({
						processing: false,
						contactPersonId: contactPerson.ContactPersonId,
						name: contactPerson.Name, 
						tel: contactPerson.Telephone,
						cell: contactPerson.Cellphone
					});
				} else {
					that.setState({ processing: false });
				}
			});
		}
  	}

	updateContactPerson= async () => {
		var contactPerson = {};
		contactPerson.ContactPersonId = this.state.contactPersonId;
        contactPerson.Name = this.state.name;
        contactPerson.Telephone = this.state.tel;
		contactPerson.Cellphone = this.state.cell;
		
		var requestFailed = true;
		this.setState({ processing: true });

		await ContactPersonActions.update(this.state.accessToken, contactPerson, function(response){
	      	if(response.Message === undefined || response.Message === ""){
  				requestFailed = false;
  				notify.show("Contact Person Successfully Updated!", "custom", 5000, { background: lightBlue600, text: "#FFFFFF" });
  			}
  			else
  			{
  				notify.show("Error: Could not update Contact Person", "custom", 15000, { background: redA400, text: "#FFFFFF" });  				
  			}
		});

		this.setState({ processing: false });
		if(!requestFailed) this.continue();
	}

	 _handleNameChange = (e) => {
        this.setState({ name: e.target.value });
    }
	 _handleTelephoneChange = (e) => {
        this.setState({ tel: e.target.value });
    }
	 _handleCellphoneChange = (e) => {
        this.setState({ cell: e.target.value });
    }

	render() {

		let formAction = null;
		if(this.state.processing){
			formAction = <CircularProgress size={60} thickness={5} />;
		} else {
			formAction = <div><RaisedButton label="Update" style={{margin: 20}} onTouchTap={this.updateContactPerson} /></div>;
								//<br /><FlatButton label="Skip" secondary={true} onClick={this.continue} /></div>;			
		}	

    	return (
				this.state.redirectToTransaction ? <Redirect to={'/Login'}/> :   		
	    		<Paper style={panel} zDepth={3}>
	    			<h4>Contact Person</h4>
	    			<TextField hintText="Name (Name/Initials & Surname)" floatingLabelText="Name" style={{marginRight: 20}} value={this.state.name} onChange={this._handleNameChange} /><br />
	    			<TextField hintText="Telephone Number" floatingLabelText="Telephone Number" style={{marginRight: 20}} value={this.state.tel} onChange={this._handleTelephoneChange} /><br />
	    			<TextField hintText="Cell Number" floatingLabelText="Cell Number" style={{marginRight: 20}} value={this.state.cell} onChange={this._handleCellphoneChange}  /><br /><br />
	    			{formAction}
	    		</Paper>
    	)
	}
}

export default UpdateContactPerson;